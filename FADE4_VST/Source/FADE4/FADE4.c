#include "FADE4.h"

#include "FM_Voice.h"
#include "FM_VoiceEnv.h"
#include "FM_ModEnv.h"
#include "FM_ModLFO.h"
#include "oscillator_wavetable.h"
#include "Synth_Parameters.h"

#define PARAMCHANGE_BUFLEN 100

int BUFFER_LENGTH;

FM_Voice Voices[NUM_VOICES];

typedef struct
{
    int param;
    float value;
}FADE4_Param;

FADE4_Param paramChangeBuffer[PARAMCHANGE_BUFLEN];
int paramChangePtr;
int updating;

void FADE4_init(float audioSR, int bufLen){
    BUFFER_LENGTH = bufLen;

    osc_initTables(audioSR);   //Init wavetables
    fm_initLUT(bufLen); //Init scaling LUT
    voiceEnv_initLUT(audioSR); //Init envelope LUT
    modEnv_initLUT(audioSR);

    initBasicFade4Patch();   //Set up params for default patch

    int v;
    for (v = 0; v < NUM_VOICES; v++){   //Init all FM Voices
        fm_initVoice(&Voices[v]);
    }

    paramChangePtr = 0;
    updating = 0;
}

void FADE4_initBasicPatch()
{
	initBasicFade4Patch();
}

void FADE4_update()
{
    updating = 1;
    for (paramChangePtr; paramChangePtr > 0; paramChangePtr--)
    {
        fm_params[paramChangeBuffer[paramChangePtr].param] = paramChangeBuffer[paramChangePtr].value;
    }
    updating = 0;

    int v;
    for (v = 0; v < NUM_VOICES; v++){
        fm_updateParams(&Voices[v]);
    }
}//Called every audio block

float FADE4_process()
{
    const static float VOICE_SCALE = 1.0 / (float)NUM_VOICES;
    float thisSample = 0;
    int v;
    for (v = 0; v < NUM_VOICES; v++){

        thisSample += fm_processSample(&Voices[v]) * (VOICE_SCALE);

        /*if (fm_voiceIsPlaying(&Voices[v]) != -1)
        {
            thisSample += fm_processSample(&Voices[v]) * (VOICE_SCALE);
        }*/
    }
    return thisSample;
}//Called every sample. Returns output sample

void FADE4_keyDown(int midiNote, int velocity)
{
    // Look for an unused synth element to play this note on
    int indx = 0;
    do
    {
        if (fm_voiceIsPlaying(&Voices[indx]) == -1) {
            fm_startPlaying(&Voices[indx], midiNote, velocity);
            return;
        }
        indx++;
    } while (indx < NUM_VOICES);
    
    //Only reachable if voice isn't found
    int max_indx = 0;
    int max_dur = 0;
    for(indx = 0; indx < NUM_VOICES; indx++)
    {
        int timePlaying = fm_voiceIsPlaying(&Voices[indx]);
        if(timePlaying > max_dur)
        {
            max_indx = indx;
            max_dur = timePlaying;
        }
    }
    fm_stopPlaying(&Voices[max_indx]);
    fm_startPlaying(&Voices[max_indx], midiNote, velocity);
}   //Call when MIDI note on message is received. Starts playing available note.

void FADE4_keyUp(int midiNote)
{
    int indx = 0;
    do
    {
        if (fm_getVoiceMidiNote(&Voices[indx]) == midiNote)
        {
            fm_stopPlaying(&Voices[indx]);
            return;
        }
        indx++;
    } while (indx < NUM_VOICES);
}//Call when MIDI note off is received. Stops playing corresponding note.

void FADE4_allOff()
{
    int v;
    for (v = 0; v < NUM_VOICES; v++)
    {
        fm_stopPlaying(&Voices[v]);
    }
}//KeyUp's every voice

void FADE4_changeParam(int index, float data)
{
    while (updating);

    if (!(paramChangePtr >= PARAMCHANGE_BUFLEN - 1))
    {
        paramChangePtr++;
        paramChangeBuffer[paramChangePtr].param = index;
        paramChangeBuffer[paramChangePtr].value = data;
    }
	//fm_params[index] = data;
}

int scaleValue(int toBeScaled, int scaleFact)
{
    long scaled = toBeScaled;
    int result = (scaled * scaleFact) >> SCALING_SHIFT;
    return result;
}   //Scales parameter values

int scaleValueGain(int toBeScaled, int gain)
{
    long scaled = toBeScaled;
    int result = (scaled * gain) >> GAIN_SHIFT;
    return result;
}   //Scales gain values

FM_Voice * FADE4_getVoiceObjs()
{
    return Voices;
}
