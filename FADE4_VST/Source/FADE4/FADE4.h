/*
 * File:   FADE4.h
 * Author: crick
 *
 * Created on July 31, 2019, 6:40 PM
 */

#ifndef FADE4_H
#define	FADE4_H

#include <stdlib.h>
#include <stdint.h>
#include <math.h>

//Synth properties
#define NUM_OPS 4
#define NUM_VOICES 6

//Scaling constants
#define GAIN_SHIFT 16
#define SCALING_SHIFT 12
#define SCALING_MAX 4095

#ifdef	__cplusplus
extern "C" {
#endif
    
    typedef enum {
        SIN,
        SQR8,
        SQR32,
        SAW8,
        SAW32
    } WAVETABLE;
    
    typedef struct {
        int8_t done;
        uint16_t atkGain;
        uint16_t relGain;
        int envState;
        uint32_t timeKeeper;
        uint32_t envPtr;
        int32_t envInc[6];
        uint16_t dests[6];
        uint32_t times[6];
        uint16_t diffs[6];
        uint16_t sources[6];
    }FM_ModEnv;
    
    typedef struct {
        int8_t envState;
        float timeKeeper;
        float envPtr;
        float relDirection;
        float initLev;
        float relGain;
        float envInc[6];
        float times[6];
        float scale[6];
        float base[6];
    }FM_OpEnv;
    
    typedef struct {
        float angleDelt;
        float curAngle;
        float lastSamples[2400];
        float lastSamplesNeg[2400];
        float envGain;
        float opFreq;
        FM_OpEnv env;
    } FM_Operator;

    typedef struct {
        float globalAngleDelt;
        int32_t playing;
        uint8_t numDone;
        uint16_t velLev;
        uint8_t midi_note;
        float freq;
        float glideInc;
        uint8_t gliding;
        float modLFOGain;
        float modEnvGain;
        int bufPtr;
        float opModulators[NUM_OPS];
        FM_Operator ops[NUM_OPS];
        FM_ModEnv modEnv;
    } FM_Voice;
    
    void FADE4_init(float audioSR, int bufLen);
    
    void FADE4_initBasicPatch();

    void FADE4_update();    //Called every audio block
    
    float FADE4_process();    //Called every sample. Returns output sample
    
    void FADE4_keyDown(int midiNote, int velocity);   //Call when MIDI note on message is received. Starts playing available note.
    
    void FADE4_keyUp(int midiNote);   //Call when MIDI note off is received. Stops playing corresponding note.
    
    void FADE4_allOff();  //KeyUp's every voice
    
    void FADE4_changeParam(int index, float data);

    int scaleValue(int toBeScaled, int scaleFact);

    int scaleValueGain(int toBeScaled, int gain);
    
    FM_Voice * FADE4_getVoiceObjs();


#ifdef	__cplusplus
}
#endif

#endif	/* FADE4_H */

