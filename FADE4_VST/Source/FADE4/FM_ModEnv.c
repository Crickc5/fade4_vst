#include "FM_ModEnv.h"

#include "Synth_Parameters.h"

//ENVELOPE CALIBRATION CONSTANTS
#define ENV_SLOPE 3 //Exponent slope of envelope curve
#define ENVTIME_MAX_SECONDS 2
#define ENVLEVS_SHIFT 16
#define ENVTIMES_SHIFT 17
#define ENVLUT_END 4095

#define ENV_INTERPOLATE 0   //Set to 0 to use no linear interpolation

static uint16_t ENVLEVS_MAX = (1 << ENVLEVS_SHIFT) - 1;
int ENVTIMES_MAX;

typedef enum
{
    ENVSTATE_ATK,
    ENVSTATE_DEC,
    ENVSTATE_SUS,
    ENVSTATE_REL,
    ENVSTATE_OFF
}ENVSTATE;

uint16_t envLUT[4096];

void modEnv_initLUT(int audioSR)
{
    ENVTIMES_MAX = audioSR * ENVTIME_MAX_SECONDS;
    int i;
    for (i = 0; i < (4096); i++){
        envLUT[i] = ((float)ENVLEVS_MAX) * powf(1 - (float)i/4096.0, (float)ENV_SLOPE);
    }
}

void modEnv_process(FM_Voice * v){
    FM_ModEnv * modEnv = &v->modEnv;
    
    //Handle changing envelope states
    if (!modEnv->done){
        if (modEnv->timeKeeper >= modEnv->times[modEnv->envState]){
            //voice->ops[op].envGain = modEnv->dests[modEnv->envState];
            modEnv->envState++;
            modEnv->timeKeeper = 0;
            modEnv->envPtr = 0;
            if (modEnv->envState == ENVSTATE_OFF){
                modEnv->done = 1;
                return;
            }
        }
        
    if (modEnv->envState == ENVSTATE_SUS)
    {
        return;
    }
    

        uint16_t indx = modEnv->envPtr >> 20;
        int y0 = envLUT[indx];
        uint16_t finalLUT;
        
#if ENV_INTERPOLATE
        int y1 = 0;
        if (indx < 4095){
            y1 = envLUT[indx + 1];
        }
        //Process Envelope at this sample
        int lowBits = (modEnv->envPtr & 0xFFFFF);
        uint16_t finalLUT = y0 + (((int64_t)(y1 - y0) * (int64_t)lowBits) >> 20);   //Linear Interpolation
#else
        finalLUT = y0;
#endif
        
        uint32_t part1 = scaleValueGain(finalLUT, modEnv->diffs[modEnv->envState]);    //Scale envelope to bounds

        if (modEnv->sources[modEnv->envState] < modEnv->dests[modEnv->envState])
        {
            modEnv->envPtr -= modEnv->envInc[modEnv->envState];
            v->modEnvGain = part1 + modEnv->sources[modEnv->envState];
        }
        else
        {
            modEnv->envPtr += modEnv->envInc[modEnv->envState];
            v->modEnvGain = part1 + modEnv->dests[modEnv->envState];
        }
        
        modEnv->timeKeeper++;
    }
}

void modEnv_init(FM_Voice * v){
    FM_ModEnv * modEnv = &v->modEnv;
    modEnv->done = 1;
    modEnv->dests[ENVSTATE_ATK] = 65535;
    modEnv->dests[ENVSTATE_SUS] = 0;
    modEnv->diffs[ENVSTATE_SUS] = 0;
    modEnv->times[ENVSTATE_SUS] = 100;
    modEnv->envInc[ENVSTATE_SUS] = 0;
    modEnv->dests[ENVSTATE_REL] = 0;
    modEnv->envPtr = 0;
    modEnv->atkGain = 0;
    modEnv->relGain = 0;
    
}

void modEnv_update(FM_Voice * v){

    FM_ModEnv * modEnv = &v->modEnv;
    modEnv->times[ENVSTATE_ATK] = scaleValue(ENVTIMES_MAX, fm_params[mod_atkTime]);
    modEnv->sources[ENVSTATE_ATK] = modEnv->atkGain;
    modEnv->diffs[ENVSTATE_ATK] = abs(modEnv->sources[ENVSTATE_ATK] - modEnv->dests[ENVSTATE_ATK]);
    modEnv->envInc[ENVSTATE_ATK] = (UINT32_MAX - 1) / (float)modEnv->times[ENVSTATE_ATK];

    modEnv->dests[ENVSTATE_DEC] = scaleValue(ENVLEVS_MAX, fm_params[mod_susLev]);
    modEnv->times[ENVSTATE_DEC] = scaleValue(ENVTIMES_MAX, fm_params[mod_decTime]);
    modEnv->sources[ENVSTATE_DEC] = modEnv->dests[ENVSTATE_ATK];
    modEnv->diffs[ENVSTATE_DEC] = abs(modEnv->sources[ENVSTATE_DEC] - modEnv->dests[ENVSTATE_DEC]);
    modEnv->envInc[ENVSTATE_DEC] = (UINT32_MAX - 1) / (float)modEnv->times[ENVSTATE_DEC];

    modEnv->sources[ENVSTATE_SUS] = modEnv->dests[ENVSTATE_DEC];

    modEnv->times[ENVSTATE_REL] = scaleValue(ENVTIMES_MAX, fm_params[mod_relTime]);
    modEnv->sources[ENVSTATE_REL] = modEnv->relGain;
    modEnv->diffs[ENVSTATE_REL] = abs(modEnv->sources[ENVSTATE_REL] - modEnv->dests[ENVSTATE_REL]);
    modEnv->envInc[ENVSTATE_REL] = (UINT32_MAX - 1) / (float)modEnv->times[ENVSTATE_REL];
    
}

void modEnv_keyDown(FM_Voice * v){
    FM_ModEnv * modEnv = &v->modEnv;
    modEnv->envState = ENVSTATE_ATK;
    modEnv->atkGain = v->modEnvGain;
    modEnv->relGain = 0;
    modEnv->timeKeeper = 0;
    modEnv->done = 0;
    modEnv->sources[ENVSTATE_ATK] = modEnv->atkGain;
    modEnv->diffs[ENVSTATE_ATK] = abs(modEnv->sources[ENVSTATE_ATK] - modEnv->dests[ENVSTATE_ATK]);
    modEnv->envInc[ENVSTATE_ATK] = (UINT32_MAX - 1) / (float)modEnv->times[ENVSTATE_ATK];

    modEnv->envPtr = UINT32_MAX;
}

void modEnv_keyUp(FM_Voice * v){
    FM_ModEnv * modEnv = &v->modEnv;
    modEnv->relGain = v->modEnvGain;
    modEnv->envState = ENVSTATE_REL;
    modEnv->timeKeeper = 0;

    modEnv->times[ENVSTATE_REL] = scaleValue(ENVTIMES_MAX, fm_params[mod_relTime]);
    modEnv->sources[ENVSTATE_REL] = modEnv->relGain;
    modEnv->diffs[ENVSTATE_REL] = abs(modEnv->sources[ENVSTATE_REL] - modEnv->dests[ENVSTATE_REL]);
    modEnv->envInc[ENVSTATE_REL] = (UINT32_MAX - 1) / (float)modEnv->times[ENVSTATE_REL];

    modEnv->envPtr = 0;
}
