/* 
 * File:   FM_ModEnv.h
 * Author: crick
 *
 * Created on June 23, 2019, 6:02 PM
 */

#ifndef FM_MODENV_H
#define	FM_MODENV_H

#include "FADE4.h"

#ifdef	__cplusplus
extern "C" {
#endif

    void modEnv_initLUT(int audioSR);

    void modEnv_process(FM_Voice * v);
    
    void modEnv_init(FM_Voice * v);
    
    void modEnv_update(FM_Voice * v);
    
    void modEnv_keyDown(FM_Voice * v);
    
    void modEnv_keyUp(FM_Voice * v);


#ifdef	__cplusplus
}
#endif

#endif	/* FM_MODENV_H */

