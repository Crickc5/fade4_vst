/* 
 * File:   FM_ModLFO.h
 * Author: crick
 *
 * Created on June 23, 2019, 6:02 PM
 */

#ifndef FM_MODLFO_H
#define	FM_MODLFO_H

#include "FADE4.h"

#ifdef	__cplusplus
extern "C" {
#endif


    void modLFO_process(FM_Voice * v);
    
    void modLFO_init(FM_Voice * v);
    
    void modLFO_update(FM_Voice * v);
    
    void modLFO_resetPosition(FM_Voice * v);


#ifdef	__cplusplus
}
#endif

#endif	/* FM_MODLFO_H */

