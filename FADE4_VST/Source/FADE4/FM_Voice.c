/*
 * File:   FM_Voice.c
 * Author: crick
 *
 * Created on June 22, 2019, 4:27 PM
 */
#include "FM_Voice.h"
#include "oscillator_wavetable.h"
#include "Synth_Parameters.h"
#include "FM_VoiceEnv.h"
#include "FM_ModEnv.h"
#include "FM_ModLFO.h"

//FM VOICE CALIBRATION CONSTANTS
#define FREQ_SCALING_MAX 4000.f
#define FREQ_SCALING_SLOPE 1.5f
#define SCALELUT_LEN 4096
#define AUTOSCALE 1
#define GLIDE_MAXTIME 2.f

static const float FREQ_SCALE = 1.f / FREQ_SCALING_MAX;

static float TABLE_DELT;
static int BUFFER_LEN;
static float scaleLUT[SCALELUT_LEN] = { 0 };

inline float fm_freqScale(float freq, float param)
{
    float scaleVal;
    float freqRatio = freq * FREQ_SCALE;
    if (freqRatio > 1.f)
    {
#if AUTOSCALE
        scaleVal = scaleLUT[(SCALELUT_LEN - 1)];
#else
        scaleVal = scaleLUT[(int)((SCALELUT_LEN - 1) * fm_params[global_scaling])];
#endif
    }
    else
    {
#if AUTOSCALE
        float scaleIndx = (SCALELUT_LEN - 1) * freqRatio;
#else
        float scaleIndx = (SCALELUT_LEN - 1) * freqRatio * fm_params[global_scaling];
#endif
        int indx0 = scaleIndx;
        int indx1 = indx0 + 1;
        if (indx1 < SCALELUT_LEN)
        {
            float diff = scaleIndx - indx0;
            scaleVal = scaleLUT[indx0] + (scaleLUT[indx1] - scaleLUT[indx0]) * diff;
        }
        else
        {
            scaleVal = scaleLUT[indx0];
        }
    }
    return param * scaleVal;
}

inline int fm_envDone(FM_Voice * v)
{
    return (v->numDone >=  NUM_OPS);
}

void fm_initLUT(int bufLen)
{
    BUFFER_LEN = bufLen;
    int i;
    for (i = 0; i < SCALELUT_LEN; i++){
        scaleLUT[i] = powf(1.f - i / 4096.f, FREQ_SCALING_SLOPE);
    }
}

void fm_initVoice(FM_Voice * v)
{
    v->velLev = 0;
    v->numDone = 0;
    v->midi_note = 0;
    v->playing = -1;
    v->freq = 1;
    v->bufPtr = 0;

    int i;
    for (i = 0; i < NUM_OPS; i++){
        v->ops[i].curAngle = 0;
        v->ops[i].envGain = 0;
        v->opModulators[i] = 0;
        int j;
        for (j = 0; j < 2400; j++)
        {
            v->ops[i].lastSamples[j] = 0;
            v->ops[i].lastSamplesNeg[j] = 0;
        }
    }

    voiceEnv_init(v);
    modEnv_init(v);
    modLFO_init(v);

    fm_updateParams(v);
}

void fm_getIncPtr(float freq)
{
    return TABLE_DELT * freq;
}

void fm_updateParams(FM_Voice * v)
{
    int o;
    for (o = 0; o < NUM_OPS; o++){
        v->ops[o].opFreq = osc_getNewFreq(v->freq * fm_params[op_1_tuneRatio + o], fm_params[op_1_coarseTune + o] + fm_params[op_1_fineTune + o]);
        v->ops[o].angleDelt = osc_getIncPtr(v->ops[o].opFreq);
    }
    voiceEnv_update(v);
    modEnv_update(v);
}

float fm_processSample(FM_Voice * v)
{
    float finalSample = 0;	//Final output sample of voice
    float opOuts[NUM_OPS] = {0};	//Audio out of each operator
    float nextOpMods[NUM_OPS] = { 0 };  //Audio modulating each operator

    float ptr = 0;
    int o;
    for (o = 0; o < NUM_OPS; o++){

        ptr = v->ops[o].curAngle;

        ///CALCULATE OUTPUT GAIN///
        voiceEnv_process(v, o);
        float lfoGain = v->modLFOGain * fm_params[lfo_toOp1OutLev + o];
        float totModGain = v->ops[o].envGain;

        ///FEEDBACK///
        float fdbkGain = (v->modLFOGain * fm_params[lfo_toOp1Fdbk + o]
                          + v->modEnvGain * fm_params[modEnv_toOp1Fdbk + o]
                          + fm_freqScale(v->ops[o].opFreq, fm_params[op_1_fdbkAmt + o]));

        float fdbkContrib = 0;
        float fdbkContribNeg = 0;
        float phaseDelay = (fm_params[op_1_fdbkPhase + o] * OSC_FS / v->ops[o].opFreq) * 0.125;
        float phaseDiff = phaseDelay - ((int)phaseDelay);
        int fdbkBufPtr = v->bufPtr - 1 - ((int)phaseDelay);
        while (fdbkBufPtr < 0)
            fdbkBufPtr += 2400;
        int fdbkBufPtr2 = (fdbkBufPtr == 0) ? 2399 : fdbkBufPtr - 1;

        if (v->playing > phaseDelay)
        {
            float samp1 = v->ops[o].lastSamples[fdbkBufPtr];
            float samp2 = v->ops[o].lastSamples[fdbkBufPtr2];
            fdbkContrib = (samp1 + (samp2 - samp1) * phaseDiff) * fdbkGain * OSC_TABLE_LEN;
            samp1 = v->ops[o].lastSamplesNeg[fdbkBufPtr];
            samp2 = v->ops[o].lastSamplesNeg[fdbkBufPtr2];
            fdbkContribNeg = (samp1 + (samp2 - samp1) * phaseDiff) * fdbkGain * OSC_TABLE_LEN;
        }
        ///SAMPLE LOOKUP///

        //Raw samples w/ amplitude processing applied
        float safeSampleSaw = osc_getSample(ptr + FDBKAMT_MAX * (v->opModulators[o] + fdbkContrib));
        float safeNegSample = osc_getSample(ptr + FDBKAMT_MAX * (v->opModulators[o] - fdbkContribNeg));

        if (fm_params[op_1_fdbkPrePost + o] == 0)
        {
            float safeSampleSqr = (safeSampleSaw + safeNegSample) * 0.72;
            opOuts[o] = (safeSampleSaw + (safeSampleSqr - safeSampleSaw) * fm_params[op_1_fdbkShape + o]) * totModGain;
        }
        else
        {
            safeSampleSaw *= totModGain;
            safeNegSample *= totModGain;
            float safeSampleSqr = (safeSampleSaw + safeNegSample) * 0.72;
            opOuts[o] = safeSampleSaw + (safeSampleSqr - safeSampleSaw) * fm_params[op_1_fdbkShape + o];
        }

        //Push samples into positive and negative feedback buffers
        v->ops[o].lastSamples[v->bufPtr] = safeSampleSaw;
        v->ops[o].lastSamplesNeg[v->bufPtr] = safeNegSample;

        finalSample += 0.25 * opOuts[o] * fm_params[op_1_outLev + o];

        v->ops[o].curAngle += v->ops[o].angleDelt; //Increment angle
        while(v->ops[o].curAngle >= OSC_TABLE_LEN)
            v->ops[o].curAngle -= OSC_TABLE_LEN;

        //Modulator Gain
        nextOpMods[(int)fm_params[op_1_FMDest + o]] += opOuts[o] * fm_params[op_1_nextFMAmt + o] * OSC_TABLE_LEN * FMAMT_MAX;
    }

    int i;
    for (i = 0; i < NUM_OPS; i++)
    {
        v->opModulators[i] = nextOpMods[i];
    }

    v->bufPtr++;
    v->bufPtr %= 2400;

    //modEnv_process(v);
    //modLFO_process(v);

    v->playing++;

    if (finalSample > 1.0)
        finalSample = 1.0;
    else if (finalSample < -1.0)
        finalSample = -1.0;

    if (fm_envDone(v))
        v->playing = -1;

    return finalSample;
}

void fm_startPlaying(FM_Voice * v, int midiNote, int velocity)
{
    v->playing = 0;
    v->midi_note = midiNote;
    v->numDone = 0;
    v->velLev = velocity;

    float newFreq = powf(2.f, (midiNote - 69) / 12.f) * 440.f;
    v->freq = newFreq;

    int i, o;
    for (o = 0; o < NUM_OPS; o++)
    {
        v->ops[o].opFreq = osc_getNewFreq(v->freq * fm_params[op_1_tuneRatio + o], fm_params[op_1_coarseTune + o] + fm_params[op_1_fineTune + o]);
        v->ops[o].angleDelt = osc_getIncPtr(v->ops[o].opFreq);
        //v->ops[o].curAngle = 0;
    }

/*    for (i = 0; i < 2400; i++)
    {
        int o;
        for (o = 0; o < NUM_OPS; o++)
        {
            v->ops[o].lastSamples[i] = 0;
            v->ops[o].lastSamplesNeg[i] = 0;
        }
        v->bufPtr = 0;
    }
*/
    fm_updateParams(v);
    voiceEnv_keyDown(v);
    modEnv_keyDown(v);
    modLFO_resetPosition(v);
}

void fm_stopPlaying(FM_Voice * v)
{
    voiceEnv_keyUp(v);
    modEnv_keyUp(v);
    v->midi_note = 0;
}

int fm_voiceIsPlaying(FM_Voice * v)
{
    return v->playing;
}

int fm_getVoiceMidiNote(FM_Voice * v)
{
    return v->midi_note;
}

