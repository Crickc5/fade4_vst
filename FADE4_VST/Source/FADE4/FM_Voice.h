/* 
 * File:   FM_Voice.h
 * Author: crick
 *
 * Created on June 22, 2019, 4:27 PM
 */

#ifndef FM_VOICE_H
#define	FM_VOICE_H

//#include <stdlib.h>
//#include <math.h>

#include "FADE4.h"

#ifdef	__cplusplus
extern "C" {
#endif
    
    FM_Voice Voices[NUM_VOICES];
    
    void fm_initLUT(int bufLen);

    void fm_initVoice(FM_Voice * v);
    
    void fm_startPlaying(FM_Voice * v, int midiNote, int velocity);
    
    void fm_stopPlaying(FM_Voice * v);
    
    int fm_voiceIsPlaying(FM_Voice * v);
    
    int fm_getVoiceMidiNote(FM_Voice * v);
    
    void fm_updateParams(FM_Voice * v);

    float fm_processSample(FM_Voice * v);



#ifdef	__cplusplus
}
#endif

#endif	/* FM_VOICE_H */

