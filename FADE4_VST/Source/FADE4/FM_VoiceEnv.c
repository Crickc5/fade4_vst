#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "FM_VoiceEnv.h"
#include "Synth_Parameters.h"

//ENVELOPE CALIBRATION CONSTANTS
#define ENV_SLOPE 2 //Exponent slope of envelope curve
#define ENVTIME_MAX_SECONDS 8.0
#define ENVLEVS_SHIFT 16
#define ENVTIMES_SHIFT 17
#define ENVLUT_END 4095
#define ENVTIME_DECLICK 96

#define ENV_INTERPOLATE 0   //Set to 0 to use no linear interpolation

#define ENVLUT_LEN 4096

//optimization parameters

typedef enum
{
    ENVSTATE_DEL,
    ENVSTATE_ATK,
    ENVSTATE_DEC,
    ENVSTATE_SUS,
    ENVSTATE_REL,
    ENVSTATE_OFF
}ENVSTATE;

static float ENVTIMES_MAX = 0;
static float envLUT[ENVLUT_LEN] = { 0 };

void voiceEnv_initLUT(float audioSR)
{
    ENVTIMES_MAX = audioSR * ENVTIME_MAX_SECONDS;
    int i;
    for (i = 0; i < ENVLUT_LEN; i++){
        envLUT[i] = powf(1.f - (float)i / 4096.f, ENV_SLOPE);
    }
}

float voiceEnv_linInterp(float ptr)
{
    int indx = ptr;
    float diff = ptr - indx;
    if (ptr < 0)
    {
        indx = 0;
        diff = 0;
    }
    else if (ptr >= ENVLUT_LEN)
    {
        indx = ENVLUT_LEN - 1;
        diff = 0;
    }
    float y0 = envLUT[indx];
    float y1 = 0;
    if (indx < ENVLUT_LEN - 1){
        y1 = envLUT[indx + 1];
    } else {
    	return y0;
    }
    //Process Envelope at this sample
    float result = (y0 + (y1 - y0) * diff);
    return result;
}

void voiceEnv_process(FM_Voice * voice, int op)
{
    FM_OpEnv * opEnv = &voice->ops[op].env;

    switch (opEnv->envState)
    {
    case ENVSTATE_DEL:
    	opEnv->timeKeeper++;
    	if (opEnv->timeKeeper >= opEnv->times[opEnv->envState])
    	{
    		opEnv->envState++;
    		opEnv->timeKeeper = 0;
    		opEnv->envPtr = ENVLUT_LEN - 1;
    		voiceEnv_initStage(voice, op);
    		return;
    	}
    	break;

    case ENVSTATE_ATK:

		opEnv->envPtr -= opEnv->envInc[opEnv->envState];
		voice->ops[op].envGain = opEnv->base[opEnv->envState] + voiceEnv_linInterp(opEnv->envPtr) * opEnv->scale[opEnv->envState];
    	opEnv->timeKeeper++;
    	break;

    case ENVSTATE_DEC:
    	opEnv->envPtr += opEnv->envInc[opEnv->envState];
    	voice->ops[op].envGain = opEnv->base[opEnv->envState] + voiceEnv_linInterp(opEnv->envPtr) * opEnv->scale[opEnv->envState];
    	opEnv->timeKeeper++;
    	break;

    case ENVSTATE_SUS:
    	voice->ops[op].envGain = opEnv->base[opEnv->envState];
    	break;

    case ENVSTATE_REL:
        opEnv->envPtr += opEnv->relDirection * opEnv->envInc[opEnv->envState];
        voice->ops[op].envGain = opEnv->base[opEnv->envState] + voiceEnv_linInterp(opEnv->envPtr) * opEnv->scale[opEnv->envState];
        opEnv->timeKeeper++;
        break;

    case ENVSTATE_OFF:
        voice->ops[op].envGain = opEnv->initLev;
        return;

    default:
        return;
    }

    if (opEnv->timeKeeper >= opEnv->times[opEnv->envState])
    {
        if (opEnv->envState == ENVSTATE_REL)
            voice->numDone++;
        opEnv->envState++;
        opEnv->timeKeeper = 0;
        opEnv->envPtr = 0;
        voiceEnv_initStage(voice, op);
    }
}

void voiceEnv_updateOp(FM_Voice * voice, int op)
{
    FM_OpEnv * opEnv = &voice->ops[op].env;

    float newTime;
    switch (opEnv->envState)
    {
    case ENVSTATE_DEL:
        opEnv->times[ENVSTATE_DEL] = ENVTIMES_MAX * fm_params[op_1_delTime + op] + ENVTIME_DECLICK;
        break;

    case ENVSTATE_ATK:
    	newTime = ENVTIMES_MAX * fm_params[op_1_atkTime + op] + ENVTIME_DECLICK;
    	if (newTime != opEnv->times[ENVSTATE_ATK])
    	{
            opEnv->times[ENVSTATE_ATK] = newTime;
            opEnv->envInc[ENVSTATE_ATK] = (opEnv->envPtr) / (float)(opEnv->times[ENVSTATE_ATK] - opEnv->timeKeeper);
    	}
      break;

    case ENVSTATE_DEC:
        opEnv->base[ENVSTATE_DEC] = fm_params[op_1_susLev + op];
        opEnv->scale[ENVSTATE_DEC] = 1.0 - opEnv->base[ENVSTATE_DEC];
        newTime = ENVTIMES_MAX * fm_params[op_1_decTime + op] + ENVTIME_DECLICK;
        if (newTime != opEnv->times[ENVSTATE_DEC])
        {
            opEnv->times[ENVSTATE_DEC] = newTime;
            opEnv->envInc[ENVSTATE_DEC] = ((ENVLUT_LEN - 1) - opEnv->envPtr) / (float)(opEnv->times[ENVSTATE_DEC] - opEnv->timeKeeper);
        }

        break;

    case ENVSTATE_SUS:
        opEnv->base[ENVSTATE_SUS] = fm_params[op_1_susLev + op];
    	break;

    case ENVSTATE_REL:
        opEnv->initLev = fm_params[op_1_initLev + op];
        opEnv->base[ENVSTATE_REL] = (opEnv->relGain > opEnv->initLev) ? opEnv->initLev : opEnv->relGain;
        opEnv->scale[ENVSTATE_REL] = fabs(opEnv->relGain - opEnv->initLev);
        opEnv->relDirection = (opEnv->relGain > opEnv->initLev) ? 1 : -1;

        newTime = ENVTIMES_MAX * fm_params[op_1_relTime + op] + ENVTIME_DECLICK;
        if (newTime != opEnv->times[ENVSTATE_REL])
        {
            opEnv->times[ENVSTATE_REL] = newTime;
            opEnv->envInc[ENVSTATE_REL] = (opEnv->relDirection == 1) ? (((ENVLUT_LEN - 1) - opEnv->envPtr) / (float)(opEnv->times[ENVSTATE_REL] - opEnv->timeKeeper)) : (opEnv->envPtr / (float)(opEnv->times[ENVSTATE_REL] - opEnv->timeKeeper));
        }

        break;

    case ENVSTATE_OFF:
    	opEnv->initLev = fm_params[op_1_initLev + op];
    	break;

    default:
    	break;
    }
}

void voiceEnv_initStage(FM_Voice * voice, int op)
{
    FM_OpEnv * opEnv = &voice->ops[op].env;
    opEnv->timeKeeper = 0;

    switch (opEnv->envState)
    {
    case ENVSTATE_DEL:
        opEnv->base[ENVSTATE_DEL] = voice->ops[op].envGain;
        opEnv->times[ENVSTATE_DEL] = ENVTIMES_MAX * fm_params[op_1_delTime + op] + ENVTIME_DECLICK;
        opEnv->envPtr = ENVLUT_LEN - 1;
        break;

    case ENVSTATE_ATK:
        opEnv->base[ENVSTATE_ATK] = voice->ops[op].envGain;
        opEnv->scale[ENVSTATE_ATK] = 1.0 - voice->ops[op].envGain;
        opEnv->times[ENVSTATE_ATK] = ENVTIMES_MAX * fm_params[op_1_atkTime + op] + ENVTIME_DECLICK;
        opEnv->envInc[ENVSTATE_ATK] = (ENVLUT_LEN - 1.0) / (float)opEnv->times[ENVSTATE_ATK];
        opEnv->envPtr = ENVLUT_LEN - 1;
        break;

    case ENVSTATE_DEC:
        opEnv->base[ENVSTATE_DEC] = fm_params[op_1_susLev + op];
        opEnv->times[ENVSTATE_DEC] = ENVTIMES_MAX * fm_params[op_1_decTime + op] + ENVTIME_DECLICK;
        opEnv->scale[ENVSTATE_DEC] = 1.0 - opEnv->base[ENVSTATE_DEC];
        opEnv->envInc[ENVSTATE_DEC] = (ENVLUT_LEN - 1) / (float)(opEnv->times[ENVSTATE_DEC]);
        opEnv->envPtr = 0;
        break;

    case ENVSTATE_SUS:
        opEnv->base[ENVSTATE_SUS] = fm_params[op_1_susLev + op];
        opEnv->envPtr = 0;
    	break;

    case ENVSTATE_REL:
    	opEnv->relGain = voice->ops[op].envGain;
        opEnv->initLev = fm_params[op_1_initLev + op];
        opEnv->base[ENVSTATE_REL] = (opEnv->relGain > opEnv->initLev) ? opEnv->initLev : opEnv->relGain;
        opEnv->times[ENVSTATE_REL] = ENVTIMES_MAX * fm_params[op_1_relTime + op] + ENVTIME_DECLICK;
        opEnv->scale[ENVSTATE_REL] = fabs(opEnv->relGain - opEnv->initLev);
        opEnv->envInc[ENVSTATE_REL] = (ENVLUT_LEN - 1.0) / (float)opEnv->times[ENVSTATE_REL];
        opEnv->relDirection = (opEnv->relGain > opEnv->initLev) ? 1 : -1;
        opEnv->envPtr = (opEnv->relDirection == 1) ? 0 : (ENVLUT_LEN - 1);
        break;

    case ENVSTATE_OFF:
    	opEnv->initLev = fm_params[op_1_initLev + op];
    	break;

    default:
    	break;
    }
}

void voiceEnv_update(FM_Voice * voice)
{
    int o;
    for (o = 0; o < NUM_OPS; o++){
    	voiceEnv_updateOp(voice, o);
    }
}

void voiceEnv_init(FM_Voice * voice){
    int o;
    for (o = 0; o < NUM_OPS; o++){
        FM_OpEnv * opEnv = &voice->ops[o].env;

        opEnv->scale[ENVSTATE_DEL] = 0;

        opEnv->scale[ENVSTATE_SUS] = 0;
        opEnv->times[ENVSTATE_SUS] = 100;
        opEnv->envInc[ENVSTATE_SUS] = 0;

        opEnv->scale[ENVSTATE_OFF] = 0;
        opEnv->base[ENVSTATE_OFF] = 0;

        opEnv->envPtr = 0;
        opEnv->envState = ENVSTATE_OFF;
    }
}

void voiceEnv_keyDown(FM_Voice * voice){

	int o;
    for (o = 0; o < NUM_OPS; o++){
        FM_OpEnv * opEnv = &voice->ops[o].env;
        opEnv->envState = ENVSTATE_DEL;
        voiceEnv_initStage(voice, o);
    }
}

void voiceEnv_keyUp(FM_Voice * voice){

    int o;
    for (o = 0; o < NUM_OPS; o++){
        FM_OpEnv * opEnv = &voice->ops[o].env;
        opEnv->envState = ENVSTATE_REL;
        voiceEnv_initStage(voice, o);
    }
}
