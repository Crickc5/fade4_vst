/* 
 * File:   FM_VoiceEnv.h
 * Author: crick
 *
 * Created on June 23, 2019, 5:56 PM
 */

#ifndef FM_VOICEENV_H
#define	FM_VOICEENV_H

#include "FADE4.h"

#ifdef	__cplusplus
extern "C" {
#endif
    
    void voiceEnv_initLUT(float audioSR);
        
    void voiceEnv_process(FM_Voice * voice, int op);
    
    void voiceEnv_updateOp(FM_Voice * voice, int op);

    void voiceEnv_update(FM_Voice * voice);

    void voiceEnv_initStage(FM_Voice * voice, int op);
        
    void voiceEnv_init(FM_Voice * voice);
    
    void voiceEnv_keyDown(FM_Voice * voice);
    
    void voiceEnv_keyUp(FM_Voice * voice);


#ifdef	__cplusplus
}
#endif

#endif	/* FM_VOICEENV_H */

