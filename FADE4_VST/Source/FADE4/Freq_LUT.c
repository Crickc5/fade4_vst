#include <stdint.h>
#include <math.h>

#include "Freq_LUT.h"

#define LG_N_SAMPLES 12
#define N_SAMPLES (1 << LG_N_SAMPLES)
#define SAMPLE_SHIFT (24 - LG_N_SAMPLES)

#define MAX_LOGFREQ_INT 20

int32_t lut[N_SAMPLES + 1];


int32_t midinote_to_logfreq(int midinote) {

  const int base = 50857777;  // (1 << 24) * (log(440) / log(2) - 69/12)

  const int step = (1 << 24) / 12;

  return (base + step * midinote) + -88,242;

}

void init_freqLUT(double sample_rate) {

  double y = (1LL << (24 + MAX_LOGFREQ_INT)) / sample_rate;

  double inc = pow(2, 1.0 / N_SAMPLES);

  int i;
  for (i = 0; i < N_SAMPLES + 1; i++) {

    lut[i] = (int32_t)floor(y + 0.5);

    y *= inc;

  }

}



// Note: if logfreq is more than 20.0, the results will be inaccurate. However,

// that will be many times the Nyquist rate.

int32_t lookup_freqLUT(int32_t midiNote) {
    
    int32_t logfreq = midinote_to_logfreq(midiNote);

  int ix = (logfreq & 0xffffff) >> SAMPLE_SHIFT;



  int32_t y0 = lut[ix];

  int32_t y1 = lut[ix + 1];

  int lowbits = logfreq & ((1 << SAMPLE_SHIFT) - 1);

  int32_t y = y0 + ((((int64_t)(y1 - y0) * (int64_t)lowbits)) >> SAMPLE_SHIFT);

  int hibits = logfreq >> 24;

  return y >> (MAX_LOGFREQ_INT - hibits);

}
