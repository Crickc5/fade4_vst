/* 
 * File:   Freq_LUT.h
 * Author: crick
 *
 * Created on July 16, 2019, 9:57 PM
 */

#ifndef FREQ_LUT_H
#define	FREQ_LUT_H

#ifdef	__cplusplus
extern "C" {
#endif


  void init_freqLUT(double sample_rate);

  int32_t lookup_freqLUT(int32_t midiNote);


#ifdef	__cplusplus
}
#endif

#endif	/* FREQ_LUT_H */

