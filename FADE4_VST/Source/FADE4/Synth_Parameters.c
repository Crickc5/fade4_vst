#include "Synth_Parameters.h"

float fm_params[NUM_PARAMS] = {0};


void initBasicFade4Patch()
{
    int i;
    for (i = 0; i < NUM_PARAMS; i++)
		{
			fm_params[i] = 0;
		}
	for (i = 0; i < NUM_OPS; i++)
		{
        fm_params[op_1_tuneRatio + i] = 1;
        fm_params[op_1_waveSelect + i] = SIN;
        fm_params[op_1_susLev + i] = 1;
		}

    fm_params[op_1_outLev] = 1;
    fm_params[op_1_FMDest] = 1;
    fm_params[op_2_FMDest] = 0;
    fm_params[op_3_FMDest] = 0;
    fm_params[op_4_FMDest] = 0;
    fm_params[lfo_freq] = 1;

    fm_params[mod_susLev] = 1;
    fm_params[global_scaling] = 0;
}
