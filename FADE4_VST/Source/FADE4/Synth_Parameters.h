/* 
 * File:   Synth_Parameters.h
 * Author: crick
 *
 * Created on June 23, 2019, 5:42 PM
 */

#ifndef SYNTH_PARAMETERS_H
#define	SYNTH_PARAMETERS_H

#include <stdlib.h>
#include "FADE4.h"

//PARAMETER MIN/MAX CONSTANTS
#define WAVESEL_MAX 5
#define LFODELAY_MAX 2000
#define FDBKAMT_MAX 0.25f
#define FMAMT_MAX 5.f
    
//NUMBER OF SYNTH PARAMETERS
#define NUM_PARAMS 163

#ifdef	__cplusplus
extern "C" {
#endif

extern float fm_params[NUM_PARAMS];

void initBasicFade4Patch();

typedef enum {
    
    dummy,

    op_1_tuneRatio,
    op_2_tuneRatio,
    op_3_tuneRatio,
    op_4_tuneRatio,

    op_1_coarseTune,
    op_2_coarseTune,
    op_3_coarseTune,
    op_4_coarseTune,

    op_1_waveSelect,
    op_2_waveSelect,
    op_3_waveSelect,
    op_4_waveSelect,
            
    op_1_FMDest,
    op_2_FMDest,
    op_3_FMDest,
    op_4_FMDest,
            
    op_1_delTime,
    op_2_delTime,
    op_3_delTime,
    op_4_delTime,

    op_1_atkTime,
    op_2_atkTime,
    op_3_atkTime,
    op_4_atkTime,

    op_1_decTime,
    op_2_decTime,
    op_3_decTime,
    op_4_decTime,

    op_1_relTime,
    op_2_relTime,
    op_3_relTime,
    op_4_relTime,

    mod_atkTime,
    mod_decTime,
    mod_relTime,

    lfo_delay,

    lfo_freq,

    op_1_outLev,
    op_2_outLev,
    op_3_outLev,
    op_4_outLev,
           
    op_1_nextFMAmt,
    op_2_nextFMAmt,
    op_3_nextFMAmt,
    op_4_nextFMAmt,

    op_1_fineTune,
    op_2_fineTune,
    op_3_fineTune,
    op_4_fineTune,

    op_1_fdbkAmt,
    op_2_fdbkAmt,
    op_3_fdbkAmt,
    op_4_fdbkAmt,
            
    op_1_fdbkPhase,
    op_2_fdbkPhase,
    op_3_fdbkPhase,
    op_4_fdbkPhase,

    op_1_fdbkPrePost,
    op_2_fdbkPrePost,
    op_3_fdbkPrePost,
    op_4_fdbkPrePost,

    op_1_fdbkShape,
    op_2_fdbkShape,
    op_3_fdbkShape,
    op_4_fdbkShape,

    op_1_initLev,
    op_2_initLev,
    op_3_initLev,
    op_4_initLev,

    op_1_susLev,
    op_2_susLev,
    op_3_susLev,
    op_4_susLev,

    mod_susLev,

    modEnv_toOp1Tune,
    modEnv_toOp2Tune,
    modEnv_toOp3Tune,
    modEnv_toOp4Tune,

    modEnv_toOp1Fdbk,
    modEnv_toOp2Fdbk,
    modEnv_toOp3Fdbk,
    modEnv_toOp4Fdbk,

    lfo_toOp1Tune,
    lfo_toOp2Tune,
    lfo_toOp3Tune,
    lfo_toOp4Tune,

    lfo_toOp1Fdbk,
    lfo_toOp2Fdbk,
    lfo_toOp3Fdbk,
    lfo_toOp4Fdbk,

    lfo_toOp1OutLev,
    lfo_toOp2OutLev,
    lfo_toOp3OutLev,
    lfo_toOp4OutLev,

    global_scaling,
    global_glide,
    global_volume

} parameters_enum;


#ifdef	__cplusplus
}
#endif

#endif	/* SYNTH_PARAMETERS_H */

