#include "oscillator_wavetable.h"

#define M_PI 3.14159265358979323846

float table[OSC_TABLE_LEN];  //Assuming that samples are gonna be going out as signed 16 bit data

float pow2Float_lut[27]; //LUT for 2 to the power of (n / 12) where n is -12 to 12

static float TABLE_DELT;
float OSC_FS;

void osc_initTables(float audioSR){
    TABLE_DELT = OSC_TABLE_LEN / audioSR;
    OSC_FS = audioSR;

    int i;
    float angle = 0;
    float angle_change = 2.0 * M_PI / (float) (OSC_TABLE_LEN - 1);
    for(i = 0; i < OSC_TABLE_LEN; i++)
    {
        // Pure Sine Wave
        table[i] = (sinf(angle));

        // update angle
        angle += angle_change;
    }

    for (i = -13; i <= 13; i++)
    {
        pow2Float_lut[i + 13] = pow(2.0, i / 12.0);
    }
}

float osc_getSample(float ptr)
{ //Linear interpolation

    while (ptr >= OSC_TABLE_LEN)
        ptr -= OSC_TABLE_LEN;
    while (ptr < 0)
        ptr += OSC_TABLE_LEN;

    int indx0 = (int)ptr;
    int indx1 = (indx0 >= OSC_TABLE_LEN - 1) ? 0 : indx0 + 1;
    float diff = ptr - indx0;

    float y0 = table[indx0];
    float y1 = table[indx1];

    return y0 + ((y1 - y0) * abs(ptr - (float)indx0));
}

float osc_getNewFreq(float baseFreq, float tuneMod)
{
    int i = tuneMod;
    float diff = tuneMod - i;
    float multiplier = pow2Float_lut[i + 13];
    if (i >= -13 && i < 13)
    {
        multiplier += diff * (pow2Float_lut[i + 14] - pow2Float_lut[i + 13]);
    }
    return baseFreq * multiplier;
}

float osc_getIncPtr(float freq)
{
    return TABLE_DELT * freq;
}
