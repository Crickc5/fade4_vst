/* 
 * File:   oscillator_wavetable.h
 * Author: crick
 *
 * Created on June 22, 2019, 3:21 PM
 */

#ifndef OSCILLATOR_WAVETABLE_H
#define	OSCILLATOR_WAVETABLE_H

#include <stdlib.h>
#include <math.h>

#include "FADE4.h"

#define OSC_TABLE_LEN 4096

#ifdef	__cplusplus
extern "C" {
#endif

    extern float OSC_FS;

    void osc_initTables(float audioSR);

    float osc_getSample(float ptr);

    float osc_getNewFreq(float baseFreq, float tuneMod);

    float osc_getIncPtr(float freq);

#ifdef	__cplusplus
}
#endif

#endif	/* OSCILLATOR_WAVETABLE_H */

