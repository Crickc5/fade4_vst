/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
#include "FADE4/FADE4.h"
#include "FADE4/Synth_Parameters.h"
//[/Headers]

#include "GlobalComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
GlobalComponent::GlobalComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    mast_vol.reset (new Slider ("mast_vol"));
    addAndMakeVisible (mast_vol.get());
    mast_vol->setRange (0, 1, 0.0001);
    mast_vol->setSliderStyle (Slider::Rotary);
    mast_vol->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    mast_vol->setColour (Slider::thumbColourId, Colours::white);
    mast_vol->addListener (this);
    mast_vol->setSkewFactor (0.7);

    mast_vol->setBounds (10, 60, 80, 80);

    mast_scale.reset (new Slider ("mast_scale"));
    addAndMakeVisible (mast_scale.get());
    mast_scale->setRange (0, 1, 0.0001);
    mast_scale->setSliderStyle (Slider::Rotary);
    mast_scale->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    mast_scale->setColour (Slider::thumbColourId, Colours::white);
    mast_scale->addListener (this);
    mast_scale->setSkewFactor (0.7);

    mast_scale->setBounds (10, 391, 80, 80);

    label.reset (new Label ("new label",
                            TRANS("GLOBAL")));
    addAndMakeVisible (label.get());
    label->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (Label::backgroundColourId, Colour (0x00ffffff));
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label->setBounds (0, 8, 100, 32);

    label3.reset (new Label ("new label",
                             TRANS("KBD SCALE")));
    addAndMakeVisible (label3.get());
    label3->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label3->setJustificationType (Justification::centred);
    label3->setEditable (false, false, false);
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label3->setBounds (10, 463, 80, 24);

    mast_glide.reset (new Slider ("mast_glide"));
    addAndMakeVisible (mast_glide.get());
    mast_glide->setRange (0, 1, 0.0001);
    mast_glide->setSliderStyle (Slider::Rotary);
    mast_glide->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    mast_glide->setColour (Slider::thumbColourId, Colours::white);
    mast_glide->addListener (this);
    mast_glide->setSkewFactor (0.7);

    mast_glide->setBounds (10, 282, 80, 80);

    label5.reset (new Label ("new label",
                             TRANS("GLIDE")));
    addAndMakeVisible (label5.get());
    label5->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label5->setJustificationType (Justification::centred);
    label5->setEditable (false, false, false);
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label5->setBounds (10, 358, 80, 24);

    label6.reset (new Label ("new label",
                             TRANS("MAST VOL")));
    addAndMakeVisible (label6.get());
    label6->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label6->setJustificationType (Justification::centred);
    label6->setEditable (false, false, false);
    label6->setColour (Label::backgroundColourId, Colour (0x00320000));
    label6->setColour (TextEditor::textColourId, Colours::black);
    label6->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label6->setBounds (10, 136, 80, 24);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

GlobalComponent::~GlobalComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    mast_vol = nullptr;
    mast_scale = nullptr;
    label = nullptr;
    label3 = nullptr;
    mast_glide = nullptr;
    label5 = nullptr;
    label6 = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void GlobalComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    {
        int x = 0, y = 0, width = 100, height = 520;
        Colour fillColour = Colours::black;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 94, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 4, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 53, width = 80, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 148, width = 6, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 85, y = 148, width = 6, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 370, width = 14, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 76, y = 370, width = 14, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 474, width = 4, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 87, y = 474, width = 4, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 0, y = 256, width = 100, height = 22;
        String text (TRANS("Synth"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 68, y = 267, width = 22, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 267, width = 22, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void GlobalComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void GlobalComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == mast_vol.get())
    {
        //[UserSliderCode_mast_vol] -- add your slider handling code here..
        FADE4_changeParam(global_volume, sliderThatWasMoved->getValue());
        //[/UserSliderCode_mast_vol]
    }
    else if (sliderThatWasMoved == mast_scale.get())
    {
        //[UserSliderCode_mast_scale] -- add your slider handling code here..
        FADE4_changeParam(global_scaling, sliderThatWasMoved->getValue());
        //[/UserSliderCode_mast_scale]
    }
    else if (sliderThatWasMoved == mast_glide.get())
    {
        //[UserSliderCode_mast_glide] -- add your slider handling code here..
        FADE4_changeParam(global_glide, sliderThatWasMoved->getValue());
        //[/UserSliderCode_mast_glide]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="GlobalComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44">
    <RECT pos="0 0 100 520" fill="solid: ff000000" hasStroke="0"/>
    <RECT pos="94 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="4 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="10 53 80 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 148 6 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="85 148 6 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 370 14 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="76 370 14 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 474 4 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="87 474 4 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="0 256 100 22" fill="solid: 86828282" hasStroke="0" text="Synth"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="68 267 22 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 267 22 1" fill="solid: 88626262" hasStroke="0"/>
  </BACKGROUND>
  <SLIDER name="mast_vol" id="540c4dfb0db40e48" memberName="mast_vol" virtualName=""
          explicitFocusOrder="0" pos="10 60 80 80" thumbcol="ffffffff"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <SLIDER name="mast_scale" id="e9a0b72efaf28c73" memberName="mast_scale"
          virtualName="" explicitFocusOrder="0" pos="10 391 80 80" thumbcol="ffffffff"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="3d34546a888df094" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="0 8 100 32" bkgCol="ffffff" edTextCol="ff000000"
         edBkgCol="0" labelText="GLOBAL" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="17721dffcb5259" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="10 463 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="KBD SCALE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <SLIDER name="mast_glide" id="388389e03fd551b8" memberName="mast_glide"
          virtualName="" explicitFocusOrder="0" pos="10 282 80 80" thumbcol="ffffffff"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="d789245cd4d3b564" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="10 358 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="GLIDE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="38374e1966b16bf6" memberName="label6" virtualName=""
         explicitFocusOrder="0" pos="10 136 80 24" bkgCol="320000" edTextCol="ff000000"
         edBkgCol="0" labelText="MAST VOL" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

