/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "MainComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
MainComponent::MainComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..




    //[/Constructor_pre]

    op1_component.reset (new OperatorComponent());
    addAndMakeVisible (op1_component.get());
    op1_component->setBounds (291, 0, 160, 520);

    op2_component.reset (new OperatorComponent());
    addAndMakeVisible (op2_component.get());
    op2_component->setBounds (443, 0, 160, 520);

    op3_component.reset (new OperatorComponent());
    addAndMakeVisible (op3_component.get());
    op3_component->setBounds (595, 0, 160, 520);

    op4_component.reset (new OperatorComponent());
    addAndMakeVisible (op4_component.get());
    op4_component->setBounds (747, 0, 160, 520);

    label2.reset (new Label ("new label",
                             TRANS("OP2")));
    addAndMakeVisible (label2.get());
    label2->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label2->setJustificationType (Justification::centred);
    label2->setEditable (false, false, false);
    label2->setColour (Label::backgroundColourId, Colour (0xa0322900));
    label2->setColour (TextEditor::textColourId, Colours::black);
    label2->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label2->setBounds (451, 8, 146, 32);

    label3.reset (new Label ("new label",
                             TRANS("OP3")));
    addAndMakeVisible (label3.get());
    label3->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label3->setJustificationType (Justification::centred);
    label3->setEditable (false, false, false);
    label3->setColour (Label::backgroundColourId, Colour (0xa70f0032));
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label3->setBounds (603, 8, 146, 32);

    label4.reset (new Label ("new label",
                             TRANS("OP4")));
    addAndMakeVisible (label4.get());
    label4->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label4->setJustificationType (Justification::centred);
    label4->setEditable (false, false, false);
    label4->setColour (Label::backgroundColourId, Colour (0xa0003032));
    label4->setColour (TextEditor::textColourId, Colours::black);
    label4->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label4->setBounds (755, 8, 146, 32);

    mixer_component.reset (new MixerComponent());
    addAndMakeVisible (mixer_component.get());
    mixer_component->setBounds (899, 0, 100, 520);

    opEnv_component.reset (new OpEnvComponent());
    addAndMakeVisible (opEnv_component.get());
    opEnv_component->setBounds (91, 0, 210, 520);

    label.reset (new Label ("new label",
                            TRANS("OP1")));
    addAndMakeVisible (label.get());
    label->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (Label::backgroundColourId, Colour (0xd7320000));
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label->setBounds (299, 8, 146, 32);

    label5.reset (new Label ("new label",
                             TRANS("DIGITAL PROGRAMMABLE\n"
                             "PARAMETRIC FEEDBACK SYNTHESIZER")));
    addAndMakeVisible (label5.get());
    label5->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label5->setJustificationType (Justification::centredLeft);
    label5->setEditable (false, false, false);
    label5->setColour (Label::backgroundColourId, Colour (0x00000000));
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label5->setBounds (75, 491, 168, 30);

    label6.reset (new Label ("new label",
                             TRANS("OP2")));
    addAndMakeVisible (label6.get());
    label6->setFont (Font ("BIZ UDGothic", 12.00f, Font::plain).withTypefaceStyle ("Regular"));
    label6->setJustificationType (Justification::centred);
    label6->setEditable (false, false, false);
    label6->setColour (Label::backgroundColourId, Colour (0xa0322900));
    label6->setColour (TextEditor::textColourId, Colours::black);
    label6->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label6->setBounds (305, 441, 24, 24);

    label7.reset (new Label ("new label",
                             TRANS("OP3")));
    addAndMakeVisible (label7.get());
    label7->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label7->setJustificationType (Justification::centred);
    label7->setEditable (false, false, false);
    label7->setColour (Label::backgroundColourId, Colour (0xa70f0032));
    label7->setColour (TextEditor::textColourId, Colours::black);
    label7->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label7->setBounds (358, 463, 24, 24);

    label8.reset (new Label ("new label",
                             TRANS("OP4")));
    addAndMakeVisible (label8.get());
    label8->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label8->setJustificationType (Justification::centred);
    label8->setEditable (false, false, false);
    label8->setColour (Label::backgroundColourId, Colour (0xa0003032));
    label8->setColour (TextEditor::textColourId, Colours::black);
    label8->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label8->setBounds (409, 441, 24, 24);

    label9.reset (new Label ("new label",
                             TRANS("OP3")));
    addAndMakeVisible (label9.get());
    label9->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label9->setJustificationType (Justification::centred);
    label9->setEditable (false, false, false);
    label9->setColour (Label::backgroundColourId, Colour (0xa70f0032));
    label9->setColour (TextEditor::textColourId, Colours::black);
    label9->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label9->setBounds (509, 463, 24, 24);

    label10.reset (new Label ("new label",
                              TRANS("OP4")));
    addAndMakeVisible (label10.get());
    label10->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label10->setJustificationType (Justification::centred);
    label10->setEditable (false, false, false);
    label10->setColour (Label::backgroundColourId, Colour (0xa0003032));
    label10->setColour (TextEditor::textColourId, Colours::black);
    label10->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label10->setBounds (561, 441, 24, 24);

    label11.reset (new Label ("new label",
                              TRANS("OP1")));
    addAndMakeVisible (label11.get());
    label11->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label11->setJustificationType (Justification::centred);
    label11->setEditable (false, false, false);
    label11->setColour (Label::backgroundColourId, Colour (0xd7320000));
    label11->setColour (TextEditor::textColourId, Colours::black);
    label11->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label11->setBounds (458, 441, 24, 24);

    label12.reset (new Label ("new label",
                              TRANS("OP4")));
    addAndMakeVisible (label12.get());
    label12->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label12->setJustificationType (Justification::centred);
    label12->setEditable (false, false, false);
    label12->setColour (Label::backgroundColourId, Colour (0xa0003032));
    label12->setColour (TextEditor::textColourId, Colours::black);
    label12->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label12->setBounds (713, 441, 24, 24);

    label13.reset (new Label ("new label",
                              TRANS("OP1")));
    addAndMakeVisible (label13.get());
    label13->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label13->setJustificationType (Justification::centred);
    label13->setEditable (false, false, false);
    label13->setColour (Label::backgroundColourId, Colour (0xd7320000));
    label13->setColour (TextEditor::textColourId, Colours::black);
    label13->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label13->setBounds (610, 441, 24, 24);

    label14.reset (new Label ("new label",
                              TRANS("OP2")));
    addAndMakeVisible (label14.get());
    label14->setFont (Font ("BIZ UDGothic", 12.00f, Font::plain).withTypefaceStyle ("Regular"));
    label14->setJustificationType (Justification::centred);
    label14->setEditable (false, false, false);
    label14->setColour (Label::backgroundColourId, Colour (0xa0322900));
    label14->setColour (TextEditor::textColourId, Colours::black);
    label14->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label14->setBounds (663, 463, 24, 24);

    label15.reset (new Label ("new label",
                              TRANS("OP1")));
    addAndMakeVisible (label15.get());
    label15->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label15->setJustificationType (Justification::centred);
    label15->setEditable (false, false, false);
    label15->setColour (Label::backgroundColourId, Colour (0xd7320000));
    label15->setColour (TextEditor::textColourId, Colours::black);
    label15->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label15->setBounds (763, 441, 24, 24);

    label16.reset (new Label ("new label",
                              TRANS("OP2")));
    addAndMakeVisible (label16.get());
    label16->setFont (Font ("BIZ UDGothic", 12.00f, Font::plain).withTypefaceStyle ("Regular"));
    label16->setJustificationType (Justification::centred);
    label16->setEditable (false, false, false);
    label16->setColour (Label::backgroundColourId, Colour (0xa0322900));
    label16->setColour (TextEditor::textColourId, Colours::black);
    label16->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label16->setBounds (816, 463, 24, 24);

    label17.reset (new Label ("new label",
                              TRANS("OP3")));
    addAndMakeVisible (label17.get());
    label17->setFont (Font ("BIZ UDGothic", 10.00f, Font::plain).withTypefaceStyle ("Regular"));
    label17->setJustificationType (Justification::centred);
    label17->setEditable (false, false, false);
    label17->setColour (Label::backgroundColourId, Colour (0xa70f0032));
    label17->setColour (TextEditor::textColourId, Colours::black);
    label17->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label17->setBounds (864, 441, 24, 24);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (1200, 540);


    //[Constructor] You can add your own custom stuff here..
    op1_component->setOpNum(0);
    op2_component->setOpNum(1);
    op3_component->setOpNum(2);
    op4_component->setOpNum(3);
    //[/Constructor]
}

MainComponent::~MainComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    op1_component = nullptr;
    op2_component = nullptr;
    op3_component = nullptr;
    op4_component = nullptr;
    label2 = nullptr;
    label3 = nullptr;
    label4 = nullptr;
    mixer_component = nullptr;
    opEnv_component = nullptr;
    label = nullptr;
    label5 = nullptr;
    label6 = nullptr;
    label7 = nullptr;
    label8 = nullptr;
    label9 = nullptr;
    label10 = nullptr;
    label11 = nullptr;
    label12 = nullptr;
    label13 = nullptr;
    label14 = nullptr;
    label15 = nullptr;
    label16 = nullptr;
    label17 = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    {
        int x = 0, y = 0, width = 1200, height = 520;
        Colour fillColour = Colours::black;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 2, y = 490, width = 75, height = 32;
        String text (TRANS("FADE-4"));
        Colour fillColour = Colours::white;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 22.00f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centredLeft, true);
    }

    {
        int x = 74, y = 496, width = 1, height = 20;
        Colour fillColour = Colours::white;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void MainComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="MainComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="1200" initialHeight="540">
  <BACKGROUND backgroundColour="ff323e44">
    <RECT pos="0 0 1200 520" fill="solid: ff000000" hasStroke="0"/>
    <TEXT pos="2 490 75 32" fill="solid: ffffffff" hasStroke="0" text="FADE-4"
          fontname="BIZ UDGothic" fontsize="22.0" kerning="0.0" bold="0"
          italic="0" justification="33"/>
    <RECT pos="74 496 1 20" fill="solid: ffffffff" hasStroke="0"/>
  </BACKGROUND>
  <JUCERCOMP name="" id="b3100f3bb143586f" memberName="op1_component" virtualName=""
             explicitFocusOrder="0" pos="291 0 160 520" sourceFile="OperatorComponent.cpp"
             constructorParams=""/>
  <JUCERCOMP name="" id="cdc2474afbd6db97" memberName="op2_component" virtualName=""
             explicitFocusOrder="0" pos="443 0 160 520" sourceFile="OperatorComponent.cpp"
             constructorParams=""/>
  <JUCERCOMP name="" id="635c6208c3243b02" memberName="op3_component" virtualName=""
             explicitFocusOrder="0" pos="595 0 160 520" sourceFile="OperatorComponent.cpp"
             constructorParams=""/>
  <JUCERCOMP name="" id="45af3ec2a18b9ef4" memberName="op4_component" virtualName=""
             explicitFocusOrder="0" pos="747 0 160 520" sourceFile="OperatorComponent.cpp"
             constructorParams=""/>
  <LABEL name="new label" id="c94bc7e8745a56e4" memberName="label2" virtualName=""
         explicitFocusOrder="0" pos="451 8 146 32" bkgCol="a0322900" edTextCol="ff000000"
         edBkgCol="0" labelText="OP2" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="cbca3dff0afeae47" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="603 8 146 32" bkgCol="a70f0032" edTextCol="ff000000"
         edBkgCol="0" labelText="OP3" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="961ab699e4b868a4" memberName="label4" virtualName=""
         explicitFocusOrder="0" pos="755 8 146 32" bkgCol="a0003032" edTextCol="ff000000"
         edBkgCol="0" labelText="OP4" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <JUCERCOMP name="" id="5e6fa5fc19c1a948" memberName="mixer_component" virtualName=""
             explicitFocusOrder="0" pos="899 0 100 520" sourceFile="MixerComponent.cpp"
             constructorParams=""/>
  <JUCERCOMP name="" id="7302e6277af1e4ee" memberName="opEnv_component" virtualName=""
             explicitFocusOrder="0" pos="91 0 210 520" sourceFile="OpEnvComponent.cpp"
             constructorParams=""/>
  <LABEL name="new label" id="3d34546a888df094" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="299 8 146 32" bkgCol="d7320000" edTextCol="ff000000"
         edBkgCol="0" labelText="OP1" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="73e94f61d3e61025" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="75 491 168 30" bkgCol="0" edTextCol="ff000000"
         edBkgCol="0" labelText="DIGITAL PROGRAMMABLE&#10;PARAMETRIC FEEDBACK SYNTHESIZER"
         editableSingleClick="0" editableDoubleClick="0" focusDiscardsChanges="0"
         fontname="BIZ UDGothic" fontsize="10.0" kerning="0.0" bold="0"
         italic="0" justification="33"/>
  <LABEL name="new label" id="c1112d10d9f2c9f6" memberName="label6" virtualName=""
         explicitFocusOrder="0" pos="305 441 24 24" bkgCol="a0322900"
         edTextCol="ff000000" edBkgCol="0" labelText="OP2" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="12.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="c77b2d848a49216c" memberName="label7" virtualName=""
         explicitFocusOrder="0" pos="358 463 24 24" bkgCol="a70f0032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP3" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="195e9e063324507a" memberName="label8" virtualName=""
         explicitFocusOrder="0" pos="409 441 24 24" bkgCol="a0003032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP4" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="85ac9ea37f011d76" memberName="label9" virtualName=""
         explicitFocusOrder="0" pos="509 463 24 24" bkgCol="a70f0032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP3" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="1230f677f82e2773" memberName="label10" virtualName=""
         explicitFocusOrder="0" pos="561 441 24 24" bkgCol="a0003032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP4" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="5e30fe3a73830cc8" memberName="label11" virtualName=""
         explicitFocusOrder="0" pos="458 441 24 24" bkgCol="d7320000"
         edTextCol="ff000000" edBkgCol="0" labelText="OP1" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="74fb1985b734da49" memberName="label12" virtualName=""
         explicitFocusOrder="0" pos="713 441 24 24" bkgCol="a0003032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP4" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="a3630306c97c61d4" memberName="label13" virtualName=""
         explicitFocusOrder="0" pos="610 441 24 24" bkgCol="d7320000"
         edTextCol="ff000000" edBkgCol="0" labelText="OP1" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="58b1fd22b43462b8" memberName="label14" virtualName=""
         explicitFocusOrder="0" pos="663 463 24 24" bkgCol="a0322900"
         edTextCol="ff000000" edBkgCol="0" labelText="OP2" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="12.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="640c509e8afc8776" memberName="label15" virtualName=""
         explicitFocusOrder="0" pos="763 441 24 24" bkgCol="d7320000"
         edTextCol="ff000000" edBkgCol="0" labelText="OP1" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="9c1c254bfbb0806d" memberName="label16" virtualName=""
         explicitFocusOrder="0" pos="816 463 24 24" bkgCol="a0322900"
         edTextCol="ff000000" edBkgCol="0" labelText="OP2" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="12.0" kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="d0645fad42c457c7" memberName="label17" virtualName=""
         explicitFocusOrder="0" pos="864 441 24 24" bkgCol="a70f0032"
         edTextCol="ff000000" edBkgCol="0" labelText="OP3" editableSingleClick="0"
         editableDoubleClick="0" focusDiscardsChanges="0" fontname="BIZ UDGothic"
         fontsize="10.0" kerning="0.0" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

