/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
#include "FADE4/FADE4.h"
#include "FADE4/Synth_Parameters.h"
//[/Headers]

#include "MixerComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
MixerComponent::MixerComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    op1_mix.reset (new Slider ("op1_mix"));
    addAndMakeVisible (op1_mix.get());
    op1_mix->setRange (0, 1, 0.0001);
    op1_mix->setSliderStyle (Slider::Rotary);
    op1_mix->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op1_mix->setColour (Slider::thumbColourId, Colour (0xff7c0909));
    op1_mix->addListener (this);
    op1_mix->setSkewFactor (0.7);

    op1_mix->setBounds (10, 60, 80, 80);

    op4_mix.reset (new Slider ("op4_mix"));
    addAndMakeVisible (op4_mix.get());
    op4_mix->setRange (0, 1, 0.0001);
    op4_mix->setSliderStyle (Slider::Rotary);
    op4_mix->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op4_mix->setColour (Slider::thumbColourId, Colour (0xff20abab));
    op4_mix->addListener (this);
    op4_mix->setSkewFactor (0.7);

    op4_mix->setBounds (10, 391, 80, 80);

    label.reset (new Label ("new label",
                            TRANS("MIX")));
    addAndMakeVisible (label.get());
    label->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (Label::backgroundColourId, Colour (0x00ffffff));
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label->setBounds (0, 8, 100, 32);

    label3.reset (new Label ("new label",
                             TRANS("OP4")));
    addAndMakeVisible (label3.get());
    label3->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label3->setJustificationType (Justification::centred);
    label3->setEditable (false, false, false);
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label3->setBounds (10, 463, 80, 24);

    op2_mix.reset (new Slider ("op2_mix"));
    addAndMakeVisible (op2_mix.get());
    op2_mix->setRange (0, 1, 0.0001);
    op2_mix->setSliderStyle (Slider::Rotary);
    op2_mix->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op2_mix->setColour (Slider::thumbColourId, Colour (0xff1f4a00));
    op2_mix->addListener (this);
    op2_mix->setSkewFactor (0.7);

    op2_mix->setBounds (10, 168, 80, 80);

    label4.reset (new Label ("new label",
                             TRANS("OP2")));
    addAndMakeVisible (label4.get());
    label4->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label4->setJustificationType (Justification::centred);
    label4->setEditable (false, false, false);
    label4->setColour (TextEditor::textColourId, Colours::black);
    label4->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label4->setBounds (10, 244, 80, 24);

    op3_mix.reset (new Slider ("op3_mix"));
    addAndMakeVisible (op3_mix.get());
    op3_mix->setRange (0, 1, 0.0001);
    op3_mix->setSliderStyle (Slider::Rotary);
    op3_mix->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op3_mix->setColour (Slider::thumbColourId, Colour (0xff270066));
    op3_mix->addListener (this);
    op3_mix->setSkewFactor (0.7);

    op3_mix->setBounds (10, 282, 80, 80);

    label5.reset (new Label ("new label",
                             TRANS("OP3")));
    addAndMakeVisible (label5.get());
    label5->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label5->setJustificationType (Justification::centred);
    label5->setEditable (false, false, false);
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label5->setBounds (10, 358, 80, 24);

    label6.reset (new Label ("new label",
                             TRANS("OP1")));
    addAndMakeVisible (label6.get());
    label6->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label6->setJustificationType (Justification::centred);
    label6->setEditable (false, false, false);
    label6->setColour (Label::backgroundColourId, Colour (0x00320000));
    label6->setColour (TextEditor::textColourId, Colours::black);
    label6->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label6->setBounds (10, 136, 80, 24);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

MixerComponent::~MixerComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    op1_mix = nullptr;
    op4_mix = nullptr;
    label = nullptr;
    label3 = nullptr;
    op2_mix = nullptr;
    label4 = nullptr;
    op3_mix = nullptr;
    label5 = nullptr;
    label6 = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void MixerComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    {
        int x = 0, y = 0, width = 100, height = 520;
        Colour fillColour = Colours::black;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 94, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 4, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 53, width = 80, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 148, width = 25, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 63, y = 148, width = 28, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 256, width = 25, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 63, y = 256, width = 28, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 370, width = 25, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 63, y = 370, width = 28, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 474, width = 25, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 63, y = 474, width = 28, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void MixerComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void MixerComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == op1_mix.get())
    {
        //[UserSliderCode_op1_mix] -- add your slider handling code here..
        FADE4_changeParam(op_1_outLev, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op1_mix]
    }
    else if (sliderThatWasMoved == op4_mix.get())
    {
        //[UserSliderCode_op4_mix] -- add your slider handling code here..
        FADE4_changeParam(op_4_outLev, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op4_mix]
    }
    else if (sliderThatWasMoved == op2_mix.get())
    {
        //[UserSliderCode_op2_mix] -- add your slider handling code here..
        FADE4_changeParam(op_2_outLev, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op2_mix]
    }
    else if (sliderThatWasMoved == op3_mix.get())
    {
        //[UserSliderCode_op3_mix] -- add your slider handling code here..
        FADE4_changeParam(op_3_outLev, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op3_mix]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="MixerComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44">
    <RECT pos="0 0 100 520" fill="solid: ff000000" hasStroke="0"/>
    <RECT pos="94 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="4 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="10 53 80 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 148 25 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="63 148 28 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 256 25 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="63 256 28 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 370 25 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="63 370 28 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 474 25 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="63 474 28 1" fill="solid: 88626262" hasStroke="0"/>
  </BACKGROUND>
  <SLIDER name="op1_mix" id="540c4dfb0db40e48" memberName="op1_mix" virtualName=""
          explicitFocusOrder="0" pos="10 60 80 80" thumbcol="ff7c0909"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <SLIDER name="op4_mix" id="e9a0b72efaf28c73" memberName="op4_mix" virtualName=""
          explicitFocusOrder="0" pos="10 391 80 80" thumbcol="ff20abab"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="3d34546a888df094" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="0 8 100 32" bkgCol="ffffff" edTextCol="ff000000"
         edBkgCol="0" labelText="MIX" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="17721dffcb5259" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="10 463 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="OP4" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <SLIDER name="op2_mix" id="d4a72facb28e57fc" memberName="op2_mix" virtualName=""
          explicitFocusOrder="0" pos="10 168 80 80" thumbcol="ff1f4a00"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="33a7b6aa99e134c1" memberName="label4" virtualName=""
         explicitFocusOrder="0" pos="10 244 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="OP2" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <SLIDER name="op3_mix" id="388389e03fd551b8" memberName="op3_mix" virtualName=""
          explicitFocusOrder="0" pos="10 282 80 80" thumbcol="ff270066"
          min="0.0" max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="d789245cd4d3b564" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="10 358 80 24" edTextCol="ff000000"
         edBkgCol="0" labelText="OP3" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="38374e1966b16bf6" memberName="label6" virtualName=""
         explicitFocusOrder="0" pos="10 136 80 24" bkgCol="320000" edTextCol="ff000000"
         edBkgCol="0" labelText="OP1" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

