/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
#include "FADE4/FADE4.h"
#include "FADE4/Synth_Parameters.h"
//[/Headers]

#include "OpEnvComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
OpEnvComponent::OpEnvComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    activeOp = 0;
    //[/Constructor_pre]

    opEnv_atk.reset (new Slider ("opEnv_atk"));
    addAndMakeVisible (opEnv_atk.get());
    opEnv_atk->setRange (0, 1, 0.0001);
    opEnv_atk->setSliderStyle (Slider::LinearVertical);
    opEnv_atk->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_atk->setColour (Slider::backgroundColourId, Colour (0xff1c252a));
    opEnv_atk->addListener (this);
    opEnv_atk->setSkewFactor (0.4);

    opEnv_atk->setBounds (10, 189, 40, 160);

    opEnv_dec.reset (new Slider ("opEnv_dec"));
    addAndMakeVisible (opEnv_dec.get());
    opEnv_dec->setRange (0, 1, 0.0001);
    opEnv_dec->setSliderStyle (Slider::LinearVertical);
    opEnv_dec->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_dec->setColour (Slider::backgroundColourId, Colour (0xff1c252a));
    opEnv_dec->addListener (this);
    opEnv_dec->setSkewFactor (0.4);

    opEnv_dec->setBounds (60, 189, 40, 160);

    opEnv_sus.reset (new Slider ("opEnv_sus"));
    addAndMakeVisible (opEnv_sus.get());
    opEnv_sus->setRange (0, 1, 0.0001);
    opEnv_sus->setSliderStyle (Slider::LinearVertical);
    opEnv_sus->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_sus->setColour (Slider::backgroundColourId, Colour (0xff1c252a));
    opEnv_sus->addListener (this);
    opEnv_sus->setSkewFactor (0.7);

    opEnv_sus->setBounds (110, 189, 40, 160);

    opEnv_rel.reset (new Slider ("opEnv_rel"));
    addAndMakeVisible (opEnv_rel.get());
    opEnv_rel->setRange (0, 1, 0.0001);
    opEnv_rel->setSliderStyle (Slider::LinearVertical);
    opEnv_rel->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_rel->setColour (Slider::backgroundColourId, Colour (0xff1c252a));
    opEnv_rel->addListener (this);
    opEnv_rel->setSkewFactor (0.4);

    opEnv_rel->setBounds (160, 189, 40, 160);

    opEnv_del.reset (new Slider ("opEnv_del"));
    addAndMakeVisible (opEnv_del.get());
    opEnv_del->setRange (0, 1, 0.0001);
    opEnv_del->setSliderStyle (Slider::Rotary);
    opEnv_del->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_del->addListener (this);
    opEnv_del->setSkewFactor (0.4);

    opEnv_del->setBounds (32, 357, 60, 60);

    opEnv_init.reset (new Slider ("opEnv_init"));
    addAndMakeVisible (opEnv_init.get());
    opEnv_init->setRange (0, 1, 0.0001);
    opEnv_init->setSliderStyle (Slider::Rotary);
    opEnv_init->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    opEnv_init->addListener (this);
    opEnv_init->setSkewFactor (0.7);

    opEnv_init->setBounds (120, 357, 60, 60);

    label2.reset (new Label ("new label",
                             TRANS("INITIAL")));
    addAndMakeVisible (label2.get());
    label2->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label2->setJustificationType (Justification::centredLeft);
    label2->setEditable (false, false, false);
    label2->setColour (TextEditor::textColourId, Colours::black);
    label2->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label2->setBounds (126, 413, 48, 24);

    label3.reset (new Label ("new label",
                             TRANS("DELAY")));
    addAndMakeVisible (label3.get());
    label3->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label3->setJustificationType (Justification::centredLeft);
    label3->setEditable (false, false, false);
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label3->setBounds (37, 413, 48, 24);

    label4.reset (new Label ("new label",
                             TRANS("ATK")));
    addAndMakeVisible (label4.get());
    label4->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label4->setJustificationType (Justification::centred);
    label4->setEditable (false, false, false);
    label4->setColour (TextEditor::textColourId, Colours::black);
    label4->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label4->setBounds (10, 165, 40, 24);

    label5.reset (new Label ("new label",
                             TRANS("DEC")));
    addAndMakeVisible (label5.get());
    label5->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label5->setJustificationType (Justification::centred);
    label5->setEditable (false, false, false);
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label5->setBounds (60, 165, 40, 24);

    label6.reset (new Label ("new label",
                             TRANS("SUS")));
    addAndMakeVisible (label6.get());
    label6->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label6->setJustificationType (Justification::centred);
    label6->setEditable (false, false, false);
    label6->setColour (TextEditor::textColourId, Colours::black);
    label6->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label6->setBounds (110, 165, 40, 24);

    label7.reset (new Label ("new label",
                             TRANS("REL")));
    addAndMakeVisible (label7.get());
    label7->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label7->setJustificationType (Justification::centred);
    label7->setEditable (false, false, false);
    label7->setColour (TextEditor::textColourId, Colours::black);
    label7->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label7->setBounds (160, 165, 40, 24);

    opEnv_selOp1.reset (new TextButton ("opEnv_selOp1"));
    addAndMakeVisible (opEnv_selOp1.get());
    opEnv_selOp1->setButtonText (TRANS("OP1"));
    opEnv_selOp1->setRadioGroupId (1);
    opEnv_selOp1->addListener (this);
    opEnv_selOp1->setColour (TextButton::buttonColourId, Colour (0xff590505));
    opEnv_selOp1->setColour (TextButton::buttonOnColourId, Colours::white);
    opEnv_selOp1->setColour (TextButton::textColourOnId, Colours::black);

    opEnv_selOp1->setBounds (10, 64, 40, 40);

    opEnv_selOp2.reset (new TextButton ("opEnv_selOp2"));
    addAndMakeVisible (opEnv_selOp2.get());
    opEnv_selOp2->setButtonText (TRANS("OP2"));
    opEnv_selOp2->setRadioGroupId (1);
    opEnv_selOp2->addListener (this);
    opEnv_selOp2->setColour (TextButton::buttonColourId, Colour (0xff012c03));
    opEnv_selOp2->setColour (TextButton::buttonOnColourId, Colours::white);
    opEnv_selOp2->setColour (TextButton::textColourOnId, Colours::black);

    opEnv_selOp2->setBounds (60, 64, 40, 40);

    opEnv_selOp3.reset (new TextButton ("opEnv_selOp3"));
    addAndMakeVisible (opEnv_selOp3.get());
    opEnv_selOp3->setButtonText (TRANS("OP3"));
    opEnv_selOp3->setRadioGroupId (1);
    opEnv_selOp3->addListener (this);
    opEnv_selOp3->setColour (TextButton::buttonColourId, Colour (0xff1d053c));
    opEnv_selOp3->setColour (TextButton::buttonOnColourId, Colours::white);
    opEnv_selOp3->setColour (TextButton::textColourOnId, Colours::black);

    opEnv_selOp3->setBounds (110, 64, 40, 40);

    opEnv_selOp4.reset (new TextButton ("opEnv_selOp4"));
    addAndMakeVisible (opEnv_selOp4.get());
    opEnv_selOp4->setButtonText (TRANS("OP4"));
    opEnv_selOp4->setRadioGroupId (1);
    opEnv_selOp4->addListener (this);
    opEnv_selOp4->setColour (TextButton::buttonColourId, Colour (0xff105e65));
    opEnv_selOp4->setColour (TextButton::buttonOnColourId, Colours::white);
    opEnv_selOp4->setColour (TextButton::textColourOnId, Colours::black);

    opEnv_selOp4->setBounds (160, 64, 40, 40);

    label.reset (new Label ("new label",
                            TRANS("MODULATION")));
    addAndMakeVisible (label.get());
    label->setFont (Font ("BIZ UDGothic", 20.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centred);
    label->setEditable (false, false, false);
    label->setColour (Label::backgroundColourId, Colour (0x00ffffff));
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label->setBounds (0, 8, 210, 32);

    opEnv_selOpAll.reset (new TextButton ("opEnv_selOpAll"));
    addAndMakeVisible (opEnv_selOpAll.get());
    opEnv_selOpAll->setButtonText (TRANS("ALL"));
    opEnv_selOpAll->setRadioGroupId (1);
    opEnv_selOpAll->addListener (this);
    opEnv_selOpAll->setColour (TextButton::buttonColourId, Colours::black);
    opEnv_selOpAll->setColour (TextButton::buttonOnColourId, Colours::white);
    opEnv_selOpAll->setColour (TextButton::textColourOffId, Colours::white);
    opEnv_selOpAll->setColour (TextButton::textColourOnId, Colours::black);

    opEnv_selOpAll->setBounds (10, 116, 190, 24);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    opEnv_selOpAll->setToggleState(true, false);
    //[/Constructor]
}

OpEnvComponent::~OpEnvComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    opEnv_atk = nullptr;
    opEnv_dec = nullptr;
    opEnv_sus = nullptr;
    opEnv_rel = nullptr;
    opEnv_del = nullptr;
    opEnv_init = nullptr;
    label2 = nullptr;
    label3 = nullptr;
    label4 = nullptr;
    label5 = nullptr;
    label6 = nullptr;
    label7 = nullptr;
    opEnv_selOp1 = nullptr;
    opEnv_selOp2 = nullptr;
    opEnv_selOp3 = nullptr;
    opEnv_selOp4 = nullptr;
    label = nullptr;
    opEnv_selOpAll = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void OpEnvComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    {
        int x = 0, y = 0, width = 210, height = 520;
        Colour fillColour = Colours::black;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 4, y = 4, width = 2, height = 486;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 204, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 53, width = 60, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 0, y = 42, width = 210, height = 22;
        String text (TRANS("Mod Select"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 140, y = 53, width = 60, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 0, y = 144, width = 210, height = 22;
        String text (TRANS("Envelope"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 130, y = 155, width = 70, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 10, y = 155, width = 70, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void OpEnvComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void OpEnvComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == opEnv_atk.get())
    {
        //[UserSliderCode_opEnv_atk] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_atkTime + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_atkTime + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_atkTime + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_atkTime + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_atkTime + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_atk]
    }
    else if (sliderThatWasMoved == opEnv_dec.get())
    {
        //[UserSliderCode_opEnv_dec] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_decTime + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_decTime + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_decTime + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_decTime + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_decTime + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_dec]
    }
    else if (sliderThatWasMoved == opEnv_sus.get())
    {
        //[UserSliderCode_opEnv_sus] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_susLev + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_susLev + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_susLev + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_susLev + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_susLev + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_sus]
    }
    else if (sliderThatWasMoved == opEnv_rel.get())
    {
        //[UserSliderCode_opEnv_rel] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_relTime + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_relTime + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_relTime + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_relTime + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_relTime + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_rel]
    }
    else if (sliderThatWasMoved == opEnv_del.get())
    {
        //[UserSliderCode_opEnv_del] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_delTime + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_delTime + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_delTime + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_delTime + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_delTime + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_del]
    }
    else if (sliderThatWasMoved == opEnv_init.get())
    {
        //[UserSliderCode_opEnv_init] -- add your slider handling code here..
        if (activeOp == 4)
        {
            FADE4_changeParam(op_1_initLev + 0, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_initLev + 1, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_initLev + 2, sliderThatWasMoved->getValue());
            FADE4_changeParam(op_1_initLev + 3, sliderThatWasMoved->getValue());
        }
        else
        {
            FADE4_changeParam(op_1_initLev + activeOp, sliderThatWasMoved->getValue());
        }
        //[/UserSliderCode_opEnv_init]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}

void OpEnvComponent::buttonClicked (Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    buttonThatWasClicked->setToggleState(true, false);
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == opEnv_selOp1.get())
    {
        //[UserButtonCode_opEnv_selOp1] -- add your button handler code here..
        activeOp = 0;
        //[/UserButtonCode_opEnv_selOp1]
    }
    else if (buttonThatWasClicked == opEnv_selOp2.get())
    {
        //[UserButtonCode_opEnv_selOp2] -- add your button handler code here..

        activeOp = 1;
        //[/UserButtonCode_opEnv_selOp2]
    }
    else if (buttonThatWasClicked == opEnv_selOp3.get())
    {
        //[UserButtonCode_opEnv_selOp3] -- add your button handler code here..
        activeOp = 2;
        //[/UserButtonCode_opEnv_selOp3]
    }
    else if (buttonThatWasClicked == opEnv_selOp4.get())
    {
        //[UserButtonCode_opEnv_selOp4] -- add your button handler code here..
        activeOp = 3;
        //[/UserButtonCode_opEnv_selOp4]
    }
    else if (buttonThatWasClicked == opEnv_selOpAll.get())
    {
        //[UserButtonCode_opEnv_selOpAll] -- add your button handler code here..
        activeOp = 4;
        //[/UserButtonCode_opEnv_selOpAll]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="OpEnvComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44">
    <RECT pos="0 0 210 520" fill="solid: ff000000" hasStroke="0"/>
    <RECT pos="4 4 2 486" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="204 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="10 53 60 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="0 42 210 22" fill="solid: 86828282" hasStroke="0" text="Mod Select"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="140 53 60 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="0 144 210 22" fill="solid: 86828282" hasStroke="0" text="Envelope"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="130 155 70 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="10 155 70 1" fill="solid: 88626262" hasStroke="0"/>
  </BACKGROUND>
  <SLIDER name="opEnv_atk" id="1e0dd1bba0d9bcc6" memberName="opEnv_atk"
          virtualName="" explicitFocusOrder="0" pos="10 189 40 160" bkgcol="ff1c252a"
          min="0.0" max="1.0" int="0.0001" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <SLIDER name="opEnv_dec" id="f7c5814ef3bbe004" memberName="opEnv_dec"
          virtualName="" explicitFocusOrder="0" pos="60 189 40 160" bkgcol="ff1c252a"
          min="0.0" max="1.0" int="0.0001" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <SLIDER name="opEnv_sus" id="5605354e269731f7" memberName="opEnv_sus"
          virtualName="" explicitFocusOrder="0" pos="110 189 40 160" bkgcol="ff1c252a"
          min="0.0" max="1.0" int="0.0001" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <SLIDER name="opEnv_rel" id="f4ae8d6eebaad425" memberName="opEnv_rel"
          virtualName="" explicitFocusOrder="0" pos="160 189 40 160" bkgcol="ff1c252a"
          min="0.0" max="1.0" int="0.0001" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <SLIDER name="opEnv_del" id="8033e2d8738683e7" memberName="opEnv_del"
          virtualName="" explicitFocusOrder="0" pos="32 357 60 60" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <SLIDER name="opEnv_init" id="673b0f4d902f9674" memberName="opEnv_init"
          virtualName="" explicitFocusOrder="0" pos="120 357 60 60" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.7"
          needsCallback="1"/>
  <LABEL name="new label" id="992064bbc317412b" memberName="label2" virtualName=""
         explicitFocusOrder="0" pos="126 413 48 24" edTextCol="ff000000"
         edBkgCol="0" labelText="INITIAL" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="5450795423a22451" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="37 413 48 24" edTextCol="ff000000"
         edBkgCol="0" labelText="DELAY" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="1917499e26fc4db8" memberName="label4" virtualName=""
         explicitFocusOrder="0" pos="10 165 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="ATK" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="996a4ab4cbb64098" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="60 165 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="DEC" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="45b4ceaa87b157e4" memberName="label6" virtualName=""
         explicitFocusOrder="0" pos="110 165 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="SUS" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="41b6690a874e71b5" memberName="label7" virtualName=""
         explicitFocusOrder="0" pos="160 165 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="REL" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <TEXTBUTTON name="opEnv_selOp1" id="b6b5cdf3001e90bb" memberName="opEnv_selOp1"
              virtualName="" explicitFocusOrder="0" pos="10 64 40 40" bgColOff="ff590505"
              bgColOn="ffffffff" textColOn="ff000000" buttonText="OP1" connectedEdges="0"
              needsCallback="1" radioGroupId="1"/>
  <TEXTBUTTON name="opEnv_selOp2" id="c9836ae92a8c6f3f" memberName="opEnv_selOp2"
              virtualName="" explicitFocusOrder="0" pos="60 64 40 40" bgColOff="ff012c03"
              bgColOn="ffffffff" textColOn="ff000000" buttonText="OP2" connectedEdges="0"
              needsCallback="1" radioGroupId="1"/>
  <TEXTBUTTON name="opEnv_selOp3" id="31c8b851b43d4036" memberName="opEnv_selOp3"
              virtualName="" explicitFocusOrder="0" pos="110 64 40 40" bgColOff="ff1d053c"
              bgColOn="ffffffff" textColOn="ff000000" buttonText="OP3" connectedEdges="0"
              needsCallback="1" radioGroupId="1"/>
  <TEXTBUTTON name="opEnv_selOp4" id="1d2e95804ade8dbf" memberName="opEnv_selOp4"
              virtualName="" explicitFocusOrder="0" pos="160 64 40 40" bgColOff="ff105e65"
              bgColOn="ffffffff" textColOn="ff000000" buttonText="OP4" connectedEdges="0"
              needsCallback="1" radioGroupId="1"/>
  <LABEL name="new label" id="3d34546a888df094" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="0 8 210 32" bkgCol="ffffff" edTextCol="ff000000"
         edBkgCol="0" labelText="MODULATION" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="20.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <TEXTBUTTON name="opEnv_selOpAll" id="5536b7be009fceab" memberName="opEnv_selOpAll"
              virtualName="" explicitFocusOrder="0" pos="10 116 190 24" bgColOff="ff000000"
              bgColOn="ffffffff" textCol="ffffffff" textColOn="ff000000" buttonText="ALL"
              connectedEdges="0" needsCallback="1" radioGroupId="1"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

