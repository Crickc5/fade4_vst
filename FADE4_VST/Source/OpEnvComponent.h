/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include <JuceHeader.h>
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class OpEnvComponent  : public Component,
                        public Slider::Listener,
                        public Button::Listener
{
public:
    //==============================================================================
    OpEnvComponent ();
    ~OpEnvComponent() override;

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void sliderValueChanged (Slider* sliderThatWasMoved) override;
    void buttonClicked (Button* buttonThatWasClicked) override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    int activeOp;
    //[/UserVariables]

    //==============================================================================
    std::unique_ptr<Slider> opEnv_atk;
    std::unique_ptr<Slider> opEnv_dec;
    std::unique_ptr<Slider> opEnv_sus;
    std::unique_ptr<Slider> opEnv_rel;
    std::unique_ptr<Slider> opEnv_del;
    std::unique_ptr<Slider> opEnv_init;
    std::unique_ptr<Label> label2;
    std::unique_ptr<Label> label3;
    std::unique_ptr<Label> label4;
    std::unique_ptr<Label> label5;
    std::unique_ptr<Label> label6;
    std::unique_ptr<Label> label7;
    std::unique_ptr<TextButton> opEnv_selOp1;
    std::unique_ptr<TextButton> opEnv_selOp2;
    std::unique_ptr<TextButton> opEnv_selOp3;
    std::unique_ptr<TextButton> opEnv_selOp4;
    std::unique_ptr<Label> label;
    std::unique_ptr<TextButton> opEnv_selOpAll;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpEnvComponent)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

