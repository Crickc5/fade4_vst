/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
#include "FADE4/FADE4.h"
#include "FADE4/Synth_Parameters.h"
//[/Headers]

#include "OperatorComponent.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
void OperatorComponent::setOpNum(int opNum)
{
    thisOp = opNum;
}
//[/MiscUserDefs]

//==============================================================================
OperatorComponent::OperatorComponent ()
{
    //[Constructor_pre] You can add your own custom stuff here..
    //[/Constructor_pre]

    op_fdbkFM.reset (new Slider ("op_fdbkFM"));
    addAndMakeVisible (op_fdbkFM.get());
    op_fdbkFM->setRange (0, 1, 0.0001);
    op_fdbkFM->setSliderStyle (Slider::Rotary);
    op_fdbkFM->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_fdbkFM->addListener (this);
    op_fdbkFM->setSkewFactor (1.3);

    op_fdbkFM->setBounds (5, 59, 80, 80);

    op_fdbkPhase.reset (new Slider ("op_fdbkPhase"));
    addAndMakeVisible (op_fdbkPhase.get());
    op_fdbkPhase->setRange (0, 1, 0.0001);
    op_fdbkPhase->setSliderStyle (Slider::Rotary);
    op_fdbkPhase->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_fdbkPhase->addListener (this);

    op_fdbkPhase->setBounds (14, 143, 60, 60);

    op_fdbkShape.reset (new Slider ("op_fdbkShape"));
    addAndMakeVisible (op_fdbkShape.get());
    op_fdbkShape->setRange (0, 1, 0.0001);
    op_fdbkShape->setSliderStyle (Slider::Rotary);
    op_fdbkShape->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_fdbkShape->addListener (this);

    op_fdbkShape->setBounds (88, 143, 60, 60);

    op_fdbkPrePost.reset (new Slider ("op_fdbkPrePost"));
    addAndMakeVisible (op_fdbkPrePost.get());
    op_fdbkPrePost->setRange (0, 1, 1);
    op_fdbkPrePost->setSliderStyle (Slider::LinearVertical);
    op_fdbkPrePost->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_fdbkPrePost->addListener (this);
    op_fdbkPrePost->setSkewFactor (0.4);

    op_fdbkPrePost->setBounds (88, 77, 60, 45);

    label.reset (new Label ("new label",
                            TRANS("AMT")));
    addAndMakeVisible (label.get());
    label->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label->setJustificationType (Justification::centredLeft);
    label->setEditable (false, false, false);
    label->setColour (TextEditor::textColourId, Colours::black);
    label->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label->setBounds (32, 86, 27, 24);

    label2.reset (new Label ("new label",
                             TRANS("PHASE")));
    addAndMakeVisible (label2.get());
    label2->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label2->setJustificationType (Justification::centredLeft);
    label2->setEditable (false, false, false);
    label2->setColour (TextEditor::textColourId, Colours::black);
    label2->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label2->setBounds (20, 195, 48, 24);

    label3.reset (new Label ("new label",
                             TRANS("PRE")));
    addAndMakeVisible (label3.get());
    label3->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label3->setJustificationType (Justification::centred);
    label3->setEditable (false, false, false);
    label3->setColour (TextEditor::textColourId, Colours::black);
    label3->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label3->setBounds (103, 115, 29, 24);

    label4.reset (new Label ("new label",
                             TRANS("SHAPE")));
    addAndMakeVisible (label4.get());
    label4->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label4->setJustificationType (Justification::centredLeft);
    label4->setEditable (false, false, false);
    label4->setColour (TextEditor::textColourId, Colours::black);
    label4->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label4->setBounds (97, 195, 40, 24);

    op_tuneCoarse.reset (new Slider ("op_tuneCoarse"));
    addAndMakeVisible (op_tuneCoarse.get());
    op_tuneCoarse->setRange (-12, 12, 1);
    op_tuneCoarse->setSliderStyle (Slider::Rotary);
    op_tuneCoarse->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_tuneCoarse->addListener (this);

    op_tuneCoarse->setBounds (14, 235, 60, 60);

    op_tuneFine.reset (new Slider ("op_tuneFine"));
    addAndMakeVisible (op_tuneFine.get());
    op_tuneFine->setRange (-1, 1, 0.0001);
    op_tuneFine->setSliderStyle (Slider::Rotary);
    op_tuneFine->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_tuneFine->addListener (this);

    op_tuneFine->setBounds (88, 250, 60, 60);

    label6.reset (new Label ("new label",
                             TRANS("COARSE")));
    addAndMakeVisible (label6.get());
    label6->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label6->setJustificationType (Justification::centredLeft);
    label6->setEditable (false, false, false);
    label6->setColour (TextEditor::textColourId, Colours::black);
    label6->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label6->setBounds (20, 319, 48, 24);

    label7.reset (new Label ("new label",
                             TRANS("FINE")));
    addAndMakeVisible (label7.get());
    label7->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label7->setJustificationType (Justification::centredLeft);
    label7->setEditable (false, false, false);
    label7->setColour (TextEditor::textColourId, Colours::black);
    label7->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label7->setBounds (97, 302, 40, 24);

    op_routAmt.reset (new Slider ("op_routAmt"));
    addAndMakeVisible (op_routAmt.get());
    op_routAmt->setRange (0, 1, 0.0001);
    op_routAmt->setSliderStyle (Slider::Rotary);
    op_routAmt->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_routAmt->addListener (this);
    op_routAmt->setSkewFactor (0.4);

    op_routAmt->setBounds (39, 361, 80, 80);

    label8.reset (new Label ("new label",
                             TRANS("FM")));
    addAndMakeVisible (label8.get());
    label8->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label8->setJustificationType (Justification::centredLeft);
    label8->setEditable (false, false, false);
    label8->setColour (TextEditor::textColourId, Colours::black);
    label8->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label8->setBounds (67, 388, 24, 24);

    op_routDest.reset (new Slider ("op_routDest"));
    addAndMakeVisible (op_routDest.get());
    op_routDest->setRange (1, 3, 1);
    op_routDest->setSliderStyle (Slider::LinearHorizontal);
    op_routDest->setTextBoxStyle (Slider::NoTextBox, false, 80, 20);
    op_routDest->addListener (this);

    op_routDest->setBounds (40, 441, 80, 24);

    label9.reset (new Label ("new label",
                             TRANS("DEST")));
    addAndMakeVisible (label9.get());
    label9->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label9->setJustificationType (Justification::centredLeft);
    label9->setEditable (false, false, false);
    label9->setColour (TextEditor::textColourId, Colours::black);
    label9->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label9->setBounds (59, 487, 40, 24);

    label5.reset (new Label ("new label",
                             TRANS("POST")));
    addAndMakeVisible (label5.get());
    label5->setFont (Font ("BIZ UDGothic", 15.00f, Font::plain).withTypefaceStyle ("Regular"));
    label5->setJustificationType (Justification::centred);
    label5->setEditable (false, false, false);
    label5->setColour (TextEditor::textColourId, Colours::black);
    label5->setColour (TextEditor::backgroundColourId, Colour (0x00000000));

    label5->setBounds (98, 64, 40, 24);

    op_tuneRatio.reset (new Slider ("op_tuneRatio"));
    addAndMakeVisible (op_tuneRatio.get());
    op_tuneRatio->setRange (1, 15, 1);
    op_tuneRatio->setSliderStyle (Slider::IncDecButtons);
    op_tuneRatio->setTextBoxStyle (Slider::NoTextBox, true, 80, 20);
    op_tuneRatio->setColour (Slider::backgroundColourId, Colours::black);
    op_tuneRatio->setColour (Slider::thumbColourId, Colours::black);
    op_tuneRatio->setColour (Slider::trackColourId, Colours::black);
    op_tuneRatio->setColour (Slider::rotarySliderFillColourId, Colours::black);
    op_tuneRatio->setColour (Slider::rotarySliderOutlineColourId, Colours::black);
    op_tuneRatio->setColour (Slider::textBoxTextColourId, Colours::black);
    op_tuneRatio->setColour (Slider::textBoxBackgroundColourId, Colour (0x00000000));
    op_tuneRatio->setColour (Slider::textBoxHighlightColourId, Colour (0x00000000));
    op_tuneRatio->setColour (Slider::textBoxOutlineColourId, Colours::black);
    op_tuneRatio->addListener (this);

    op_tuneRatio->setBounds (20, 293, 48, 20);


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

OperatorComponent::~OperatorComponent()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    //[/Destructor_pre]

    op_fdbkFM = nullptr;
    op_fdbkPhase = nullptr;
    op_fdbkShape = nullptr;
    op_fdbkPrePost = nullptr;
    label = nullptr;
    label2 = nullptr;
    label3 = nullptr;
    label4 = nullptr;
    op_tuneCoarse = nullptr;
    op_tuneFine = nullptr;
    label6 = nullptr;
    label7 = nullptr;
    op_routAmt = nullptr;
    label8 = nullptr;
    op_routDest = nullptr;
    label9 = nullptr;
    label5 = nullptr;
    op_tuneRatio = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void OperatorComponent::paint (Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (Colour (0xff323e44));

    {
        int x = 0, y = 0, width = 162, height = 520;
        Colour fillColour = Colours::black;
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 4, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 156, y = 4, width = 2, height = 508;
        Colour fillColour = Colour (0xff828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 12, y = 355, width = 44, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 48, y = 344, width = 64, height = 22;
        String text (TRANS("Routing"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 106, y = 355, width = 44, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 12, y = 229, width = 50, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 48, y = 218, width = 64, height = 22;
        String text (TRANS("Tune"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 99, y = 229, width = 50, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 12, y = 53, width = 36, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    {
        int x = 48, y = 42, width = 64, height = 22;
        String text (TRANS("Feedback"));
        Colour fillColour = Colour (0x86828282);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.setFont (Font ("BIZ UDGothic", 10.70f, Font::plain).withTypefaceStyle ("Regular"));
        g.drawText (text, x, y, width, height,
                    Justification::centred, true);
    }

    {
        int x = 114, y = 53, width = 36, height = 1;
        Colour fillColour = Colour (0x88626262);
        //[UserPaintCustomArguments] Customize the painting arguments here..
        //[/UserPaintCustomArguments]
        g.setColour (fillColour);
        g.fillRect (x, y, width, height);
    }

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void OperatorComponent::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void OperatorComponent::sliderValueChanged (Slider* sliderThatWasMoved)
{
    //[UsersliderValueChanged_Pre]
    //[/UsersliderValueChanged_Pre]

    if (sliderThatWasMoved == op_fdbkFM.get())
    {
        //[UserSliderCode_op_fdbkFM] -- add your slider handling code here..
        FADE4_changeParam(op_1_fdbkAmt +  thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_fdbkFM]
    }
    else if (sliderThatWasMoved == op_fdbkPhase.get())
    {
        //[UserSliderCode_op_fdbkPhase] -- add your slider handling code here..
        FADE4_changeParam(op_1_fdbkPhase + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_fdbkPhase]
    }
    else if (sliderThatWasMoved == op_fdbkShape.get())
    {
        //[UserSliderCode_op_fdbkShape] -- add your slider handling code here..
        FADE4_changeParam(op_1_fdbkShape + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_fdbkShape]
    }
    else if (sliderThatWasMoved == op_fdbkPrePost.get())
    {
        //[UserSliderCode_op_fdbkPrePost] -- add your slider handling code here..
        FADE4_changeParam(op_1_fdbkPrePost + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_fdbkPrePost]
    }
    else if (sliderThatWasMoved == op_tuneCoarse.get())
    {
        //[UserSliderCode_op_tuneCoarse] -- add your slider handling code here..
        FADE4_changeParam(op_1_coarseTune + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_tuneCoarse]
    }
    else if (sliderThatWasMoved == op_tuneFine.get())
    {
        //[UserSliderCode_op_tuneFine] -- add your slider handling code here..
        FADE4_changeParam(op_1_fineTune + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_tuneFine]
    }
    else if (sliderThatWasMoved == op_routAmt.get())
    {
        //[UserSliderCode_op_routAmt] -- add your slider handling code here..
        FADE4_changeParam(op_1_nextFMAmt + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_routAmt]
    }
    else if (sliderThatWasMoved == op_routDest.get())
    {
        //[UserSliderCode_op_routDest] -- add your slider handling code here..
        int val = sliderThatWasMoved->getValue();
        switch (thisOp)
        {
        case 0:
            FADE4_changeParam(op_1_FMDest + thisOp, val);
            break;

        case 1:
            if (val == 1)
                FADE4_changeParam(op_1_FMDest + thisOp, 0);
            else
                FADE4_changeParam(op_1_FMDest + thisOp, val);
            break;

        case 2:
            if (val == 2)
                FADE4_changeParam(op_1_FMDest + thisOp, 1);
            else if (val == 1)
                FADE4_changeParam(op_1_FMDest + thisOp, 0);
            else
                FADE4_changeParam(op_1_FMDest + thisOp, val);
            break;

        case 3:
            FADE4_changeParam(op_1_FMDest + thisOp, val - 1);
            break;
        }
        //[/UserSliderCode_op_routDest]
    }
    else if (sliderThatWasMoved == op_tuneRatio.get())
    {
        //[UserSliderCode_op_tuneRatio] -- add your slider handling code here..
        FADE4_changeParam(op_1_tuneRatio + thisOp, sliderThatWasMoved->getValue());
        //[/UserSliderCode_op_tuneRatio]
    }

    //[UsersliderValueChanged_Post]
    //[/UsersliderValueChanged_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="OperatorComponent" componentName=""
                 parentClasses="public Component" constructorParams="" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44">
    <RECT pos="0 0 162 520" fill="solid: ff000000" hasStroke="0"/>
    <RECT pos="4 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="156 4 2 508" fill="solid: ff828282" hasStroke="0"/>
    <RECT pos="12 355 44 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="48 344 64 22" fill="solid: 86828282" hasStroke="0" text="Routing"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="106 355 44 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="12 229 50 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="48 218 64 22" fill="solid: 86828282" hasStroke="0" text="Tune"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="99 229 50 1" fill="solid: 88626262" hasStroke="0"/>
    <RECT pos="12 53 36 1" fill="solid: 88626262" hasStroke="0"/>
    <TEXT pos="48 42 64 22" fill="solid: 86828282" hasStroke="0" text="Feedback"
          fontname="BIZ UDGothic" fontsize="10.7" kerning="0.0" bold="0"
          italic="0" justification="36"/>
    <RECT pos="114 53 36 1" fill="solid: 88626262" hasStroke="0"/>
  </BACKGROUND>
  <SLIDER name="op_fdbkFM" id="540c4dfb0db40e48" memberName="op_fdbkFM"
          virtualName="" explicitFocusOrder="0" pos="5 59 80 80" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1.3"
          needsCallback="1"/>
  <SLIDER name="op_fdbkPhase" id="8033e2d8738683e7" memberName="op_fdbkPhase"
          virtualName="" explicitFocusOrder="0" pos="14 143 60 60" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1.0"
          needsCallback="1"/>
  <SLIDER name="op_fdbkShape" id="8cd867a77d347337" memberName="op_fdbkShape"
          virtualName="" explicitFocusOrder="0" pos="88 143 60 60" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1.0"
          needsCallback="1"/>
  <SLIDER name="op_fdbkPrePost" id="6d8b639d8c1a1d70" memberName="op_fdbkPrePost"
          virtualName="" explicitFocusOrder="0" pos="88 77 60 45" min="0.0"
          max="1.0" int="1.0" style="LinearVertical" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <LABEL name="new label" id="b4b7208ff37498bb" memberName="label" virtualName=""
         explicitFocusOrder="0" pos="32 86 27 24" edTextCol="ff000000"
         edBkgCol="0" labelText="AMT" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="992064bbc317412b" memberName="label2" virtualName=""
         explicitFocusOrder="0" pos="20 195 48 24" edTextCol="ff000000"
         edBkgCol="0" labelText="PHASE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="e3380933b23e6ea5" memberName="label3" virtualName=""
         explicitFocusOrder="0" pos="103 115 29 24" edTextCol="ff000000"
         edBkgCol="0" labelText="PRE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <LABEL name="new label" id="b09a75a36649f2eb" memberName="label4" virtualName=""
         explicitFocusOrder="0" pos="97 195 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="SHAPE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <SLIDER name="op_tuneCoarse" id="ce5cb4efa6220e1e" memberName="op_tuneCoarse"
          virtualName="" explicitFocusOrder="0" pos="14 235 60 60" min="-12.0"
          max="12.0" int="1.0" style="Rotary" textBoxPos="NoTextBox" textBoxEditable="1"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1.0" needsCallback="1"/>
  <SLIDER name="op_tuneFine" id="ee006b46f1f500ee" memberName="op_tuneFine"
          virtualName="" explicitFocusOrder="0" pos="88 250 60 60" min="-1.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1.0"
          needsCallback="1"/>
  <LABEL name="new label" id="1d35a85560174821" memberName="label6" virtualName=""
         explicitFocusOrder="0" pos="20 319 48 24" edTextCol="ff000000"
         edBkgCol="0" labelText="COARSE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="fd3caa68865c1775" memberName="label7" virtualName=""
         explicitFocusOrder="0" pos="97 302 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="FINE" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <SLIDER name="op_routAmt" id="4e1ced1ddd6a94cc" memberName="op_routAmt"
          virtualName="" explicitFocusOrder="0" pos="39 361 80 80" min="0.0"
          max="1.0" int="0.0001" style="Rotary" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="0.4"
          needsCallback="1"/>
  <LABEL name="new label" id="4d489a202f9036c1" memberName="label8" virtualName=""
         explicitFocusOrder="0" pos="67 388 24 24" edTextCol="ff000000"
         edBkgCol="0" labelText="FM" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <SLIDER name="op_routDest" id="1a994fac85bd7df4" memberName="op_routDest"
          virtualName="" explicitFocusOrder="0" pos="40 441 80 24" min="1.0"
          max="3.0" int="1.0" style="LinearHorizontal" textBoxPos="NoTextBox"
          textBoxEditable="1" textBoxWidth="80" textBoxHeight="20" skewFactor="1.0"
          needsCallback="1"/>
  <LABEL name="new label" id="14d869651cc1385d" memberName="label9" virtualName=""
         explicitFocusOrder="0" pos="59 487 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="DEST" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="33"/>
  <LABEL name="new label" id="14d43ede2f51b324" memberName="label5" virtualName=""
         explicitFocusOrder="0" pos="98 64 40 24" edTextCol="ff000000"
         edBkgCol="0" labelText="POST" editableSingleClick="0" editableDoubleClick="0"
         focusDiscardsChanges="0" fontname="BIZ UDGothic" fontsize="15.0"
         kerning="0.0" bold="0" italic="0" justification="36"/>
  <SLIDER name="op_tuneRatio" id="78e8fc36a9295365" memberName="op_tuneRatio"
          virtualName="" explicitFocusOrder="0" pos="20 293 48 20" bkgcol="ff000000"
          thumbcol="ff000000" trackcol="ff000000" rotarysliderfill="ff000000"
          rotaryslideroutline="ff000000" textboxtext="ff000000" textboxbkgd="0"
          textboxhighlight="0" textboxoutline="ff000000" min="1.0" max="15.0"
          int="1.0" style="IncDecButtons" textBoxPos="NoTextBox" textBoxEditable="0"
          textBoxWidth="80" textBoxHeight="20" skewFactor="1.0" needsCallback="1"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

