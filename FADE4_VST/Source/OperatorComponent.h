/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.4.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2017 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include <JuceHeader.h>
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class OperatorComponent  : public Component,
                           public Slider::Listener
{
public:
    //==============================================================================
    OperatorComponent ();
    ~OperatorComponent() override;

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    void setOpNum(int opNum);
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void sliderValueChanged (Slider* sliderThatWasMoved) override;



private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    int thisOp;
    //[/UserVariables]

    //==============================================================================
    std::unique_ptr<Slider> op_fdbkFM;
    std::unique_ptr<Slider> op_fdbkPhase;
    std::unique_ptr<Slider> op_fdbkShape;
    std::unique_ptr<Slider> op_fdbkPrePost;
    std::unique_ptr<Label> label;
    std::unique_ptr<Label> label2;
    std::unique_ptr<Label> label3;
    std::unique_ptr<Label> label4;
    std::unique_ptr<Slider> op_tuneCoarse;
    std::unique_ptr<Slider> op_tuneFine;
    std::unique_ptr<Label> label6;
    std::unique_ptr<Label> label7;
    std::unique_ptr<Slider> op_routAmt;
    std::unique_ptr<Label> label8;
    std::unique_ptr<Slider> op_routDest;
    std::unique_ptr<Label> label9;
    std::unique_ptr<Label> label5;
    std::unique_ptr<Slider> op_tuneRatio;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OperatorComponent)
};

//[EndFile] You can add extra defines here...
//[/EndFile]

