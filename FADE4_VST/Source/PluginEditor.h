/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "MainComponent.h"

//==============================================================================
/**
*/
class Fade4_vstAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    Fade4_vstAudioProcessorEditor (Fade4_vstAudioProcessor&);
    ~Fade4_vstAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Fade4_vstAudioProcessor& processor;
    MainComponent mainWindow;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Fade4_vstAudioProcessorEditor)
};
