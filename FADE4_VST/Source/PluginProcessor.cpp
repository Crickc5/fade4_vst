/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Fade4_vstAudioProcessor::Fade4_vstAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

Fade4_vstAudioProcessor::~Fade4_vstAudioProcessor()
{
}

//==============================================================================
const String Fade4_vstAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Fade4_vstAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Fade4_vstAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Fade4_vstAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Fade4_vstAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Fade4_vstAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Fade4_vstAudioProcessor::getCurrentProgram()
{
    return 0;
}

void Fade4_vstAudioProcessor::setCurrentProgram (int index)
{
}

const String Fade4_vstAudioProcessor::getProgramName (int index)
{
    return {};
}

void Fade4_vstAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Fade4_vstAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
  FADE4_init((float) sampleRate, samplesPerBlock);
}

void Fade4_vstAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Fade4_vstAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void Fade4_vstAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    FADE4_update();

    updateMidi(midiMessages);

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    auto* channelData = buffer.getWritePointer (0);
    auto* channelData2 = buffer.getWritePointer(1);

    for (int samp = 0; samp < buffer.getNumSamples(); samp++)
      {
        //int16_t outSampInt = FADE4_process();
        //float outSampFloat = outSampInt / 32767.0;
        float outSampFloat = FADE4_process();
        channelData[samp] = outSampFloat;
        channelData2[samp] = outSampFloat;
      }
}

void Fade4_vstAudioProcessor::updateMidi(MidiBuffer& midiMessages)
{
  int time;
  MidiMessage m;

  for (MidiBuffer::Iterator i(midiMessages); i.getNextEvent(m, time);)
    {
      if (m.isNoteOn())
        {
          FADE4_keyDown(m.getNoteNumber(), m.getVelocity());
        }
      else if (m.isNoteOff())
        {
          FADE4_keyUp(m.getNoteNumber());
        }
    }
}

//==============================================================================
bool Fade4_vstAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Fade4_vstAudioProcessor::createEditor()
{
    return new Fade4_vstAudioProcessorEditor (*this);
}

//==============================================================================
void Fade4_vstAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void Fade4_vstAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Fade4_vstAudioProcessor();
}
